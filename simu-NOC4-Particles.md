---
title: simu-NOC4-Particles
date: 2020-05-18 15:43:08
tags:
- particle systems
- simulation
categories:
- Simulation
---

# Particle Systems

粒子系統主要模擬煙霧、煙火等。在許多動畫設計軟體中是一項主要的工具。本章從一個基本粒子出發，學習一個粒子系統的程式設計。



## A single Particle

物理世界中，模擬一個粒子包含 position, velocity, acceleration 以及 lifespan (存在期) 。

```
class Particle {

  constructor(position) {
    this.acceleration = createVector(0, 0.05);
    this.velocity = createVector(random(-1, 1), random(-1, 0));
    this.position = position.copy();
    this.lifespan = 255.0;
  }

  run() {
    this.update();
    this.display();
  }

  // Method to update position
  update() {
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
    this.lifespan -= 2;
  }

  // Method to display
  display() {
    stroke(255, this.lifespan);
    strokeWeight(2);
    fill(127, this.lifespan);
    ellipse(this.position.x, this.position.y, 12, 12);
  }

  // Is the particle still useful?
  isDead() {
    if (this.lifespan < 0.0) {
      return true;
    } else {
      return false;
    }
  }
}
```

```
let p;

function setup() {
  createCanvas(640, 360);
  p = new Particle(createVector(width / 2, 20));
}

function draw() {
  background(51);

  p.run();
  if (p.isDead()) {
    p = new Particle(createVector(width / 2, 20));
    //println("Particle dead!");
  }
}
```

用一個陣列來模擬多個粒子：

```
let ps=[]

function draw(){
  background(51);
  particles.push(new Particle(createVector(width / 2, 50)));
  
	for( let i = ps.length-1; i >=0; i--){
      let p = ps[i]
      p.run()
      if( p.isDead())
        ps.splice(i, 1)
    }
}

```

一個煙火由一束粒子來模擬，因此我們可以將一束粒子寫成一個類別：

```
class ParticleSystem{
  constructor(position){
    this.origin = position.copy();
    this.ps = []
  }
  addParticle(){
    this.ps.push( new Particle(this.origin))
  }
  run(){
    for(let p of this.ps)
      p.run()
      
    this.ps = this.ps.filter(p=>!p.isDead())
  }
}
```

sketch.js

```
let ps;

function setup() {
  createCanvas(640, 360);
  ps = new ParticleSystem(createVector(width / 2, 50));
}

function draw() {
  background(51);
  ps.addParticle();
  ps.run();
}
```

system of systems

```
let systems = [];

function setup() {
  let text = createP("click to add particle systems");
  text.position(10, 365);

  createCanvas(640, 360);
}

function draw() {
  background(51);
  for (let i = 0; i < systems.length; i++) {
    systems[i].addParticle();
    systems[i].run();
  }
}

function mousePressed() {
  systems.push(new ParticleSystem(1, createVector(mouseX, mouseY)));
}
```

繼承與多形是物件導向程式設計一個很好的特色，我們可以從一個類別繼承設計多個類別，然後利用多形的性質來直接運用這些不同類別但同一個父類別的物件。

下面這個範例，我們從 Particle 繼承設計一個 Confetti  類別，主要差別在於顯示成會旋轉的方形：

```
class Confetti extends Particle {

  // Override the display method
  display() {
    rectMode(CENTER);
    fill(255, this.lifespan);
    stroke(255, this.lifespan);
    strokeWeight(2);
    push();
    translate(this.position.x, this.position.y);
    var theta = map(this.position.x, 0, width, 0, TWO_PI * 2);
    rotate(theta);
    rect(0, 0, 12, 12);
    pop();
  }
}
```

我們稍微改寫 ParticleSystem

```
class ParticleSystem {

  constructor(position) {
    this.origin = position.copy();
    this.particles = [];
  }

  addParticle() {
    let r = random(1);
    if (r < 0.5) {
      this.particles.push(new Particle(this.origin));
    } else {
      this.particles.push(new Confetti(this.origin));
    }
  }

  run() {
    // Run every particle
    // ES6 for..of loop
    for (let particle of this.particles) {
      particle.run();
    }

    // Filter removes any elements of the array that do not pass the test
    this.particles = this.particles.filter(particle => !particle.isDead());
  }
}
```

如此，所產生的粒子就可能有圓形跟方形二類。

## 受到力的粒子系統

模擬粒子在空中受到風力或其他外力影響，我們需要在 particle 中增加 applyForce 函式。然後在 update中增加 acceleration 的工作。

```
  applyForce(force) {
    let f = force.copy();
    f.div(this.mass);
    this.acceleration.add(f);
  }

  // Method to update position
  update() {
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
    this.lifespan -= 2.0;
  }
```

particlesystem 中也需要增加 applyForce()

```
  applyForce(f) {
    for (let i = 0; i < this.particles.length; i++) {
      this.particles[i].applyForce(f);
    }
  }
```

### 排斥力模擬

repeller 是一個類似輻射力的光源，粒子靠近時受到相反方向的排斥力量。

Repeller 的類別設計可以是：

```
class Repeller {
  constructor(x, y) {
    this.power = 150;
    this.position = createVector(x, y);
  }

  display() {
    stroke(255);
    strokeWeight(2);
    fill(127);
    ellipse(this.position.x, this.position.y, 32, 32);
  }

  repel(p) {
    let dir = p5.Vector.sub(this.position, p.position); // Calculate direction of force
    let d = dir.mag(); // Distance between objects
    dir.normalize(); 
    d = constrain(d, 1, 100); // Keep distance within a reasonable range
    let force = -1 * this.power / (d * d);  // 距離平方成反比
    dir.mult(force); // Get force vector --> magnitude * direction
    return dir;
  }
}
```

particle 還是用 applyForce 來模擬就可以了，因此我們只需要更動 particleSystem

```
  applyRepeller(r) {
    for (let particle of this.particles) {
      let force = r.repel(particle);
      particle.applyForce(force);
    }
  }
```

sketch.js 中也要加上 applyRepeller 

```js
let ps;
let repeller;

function setup() {
  createCanvas(640, 360);
  ps = new ParticleSystem(createVector(width / 2, 50));
  repeller = new Repeller(width / 2, height / 2);
}

function draw() {
  background(51);
  ps.addParticle(mouseX, mouseY);

  // Apply gravity force to all Particles
  let gravity = createVector(0, 0.02);
  ps.applyForce(gravity);

  ps.applyRepeller(repeller);

  repeller.display();
  ps.run();

}
```

## 煙霧

煙霧或煙火的模擬，最終還是需要影像設計來協助，

```
class Particle {

  constructor(pos, img) {
    this.acc = createVector(0, 0);
    let vx = randomGaussian() * 0.3;
    let vy = randomGaussian() * 0.3 - 1.0;
    this.vel = createVector(vx, vy);
    this.pos = pos.copy();
    this.lifespan = 100.0;
    this.img = img;
  }

  run() {
    this.update();
    this.render();
  }

  // Method to apply a force vector to the Particle object
  // Note we are ignoring "mass" here
  applyForce(f) {
    this.acc.add(f);
  }

  // Method to update position
  update() {
    this.vel.add(this.acc);
    this.pos.add(this.vel);
    this.lifespan -= 2.5;
    this.acc.mult(0); // clear Acceleration
  }

  // Method to display
  render() {
    imageMode(CENTER);
    tint(255, this.lifespan);     // 隨著時間逐漸透明 
    image(img, this.pos.x, this.pos.y);
  }

  // Is the particle still useful?
  isDead() {
    if (this.lifespan <= 0.0) {
      return true;
    } else {
      return false;
    }
  }
}
```

本範例使用方向風來展示：

```js
let ps;
let img;

function preload() {
  img = loadImage("data/texture.png");
}

function setup() {
  createCanvas(640, 360);
  ps = new ParticleSystem(0, createVector(width / 2, height - 75), img);
}

function draw() {

  // Try additive blending!
  // You also need clear or else the colors will accumulate between frames
  // blendMode(ADD);
  // clear();

  background(0);

  // Additive blending!
  // Calculate a "wind" force based on mouse horizontal position
  let dx = map(mouseX, 0, width, -0.2, 0.2);
  let wind = createVector(dx, 0);
  ps.applyForce(wind);
  ps.run();
  for (let i = 0; i < 2; i++) {
    ps.addParticle();
  }

  // Draw an arrow representing the wind force
  drawVector(wind, createVector(width / 2, 50, 0), 500);

}

// Renders a vector object 'v' as an arrow and a position 'loc'
function drawVector(v, pos, scayl) {
  push();
  let arrowsize = 4;
  // Translate to position to render vector
  translate(pos.x, pos.y);
  stroke(255);
  // Call vector heading function to get direction (note that pointing up is a heading of 0) and rotate
  rotate(v.heading());
  // Calculate length of vector & scale it to be bigger or smaller if necessary
  let len = v.mag() * scayl;
  // Draw three lines to make an arrow (draw pointing up since we've rotate to the proper direction)
  line(0, 0, len, 0);
  line(len, 0, len - arrowsize, +arrowsize / 2);
  line(len, 0, len - arrowsize, -arrowsize / 2);
  pop();
}
```

texture.png 可以由 gitlab.com/pcchen001/multimedia-programming-108b/examples/NatureOfCode/ch04_systems/NOC_4_08_ParticleSystemSmoke/ 下找到。

