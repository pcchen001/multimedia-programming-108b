---
title: 課程綱要
date: 2020-02-01 20:47:55
tags: 
- readme
categories:
- 多媒體程式設計
mathjax: true 
---
# Multimedia Programming 108b

A Repository for Multimedia Programming, IM.NUU

課程綱要

##### A.   Drawing

1. 2D Drawing Primitives
2. Color
3. Curves
4. Text & Typography
5. Images
6. Transform 
7. 練習
   1. 以 2D drawing primitives 配合 for 迴圈設計幾何圖案
   2. 加上色彩變化( RGB & HSB )
   3. 利用 Curves, 繪出燭光、樹葉、以及花辮
   4. 設計含有中英文文字的海報
   5. 設計含有中英文文字、影像的海報 
8. 作業
   1. 製作一張宣傳海報(靜態)，主題是 [遊樂園 X 春天]。內容包含文字、小於 30%全畫面的影像，幾何圖案，Bezier Curve 為基礎的幾何圖案，彩色。海報大小為  1920X1080 或 1080X1920 。畫面呈現在網頁的 canvas 上。  繳交期限： 3/17 。

##### B.   3D Drawing 

1. WEBGL rendering

2. Vertex and Texture

3. 3D Drawing Primitives

4. Lights & Materials

5. Shader (briefly introduction)

6. Transform (in 3D case)

7. 練習

​            i.       vertex 練習

​           ii.       利用 vertex 繪製正多邊形、星芒形、條狀形 ( 基礎幾何函式 )

​          iii.       加上 texture

​           iv.       3D primitives 練習、加上 texture

​           v.       使用 vertex 設計自訂的 3D Shapes, 並且加上 texture. (骰子設計)

​           vi.       lights & materials

​          vii.       行星運行模擬, 風車轉動模擬

8. 作業

​            i.       製作一個空間模型，包含方柱、圓柱、風車、路燈、幾何樹及地面。

##### C.   Animation

1. PVector
2. Motions of a ball
3. Motions of an arrow (directional)
4. Motions with image
5. Motions in cycle ( circle, sin, 鐘擺)
6. frameCount & frameSave
7. 練習
8. 作業

##### D.   Interaction & DOM

1. Mouse、Keyboard
2. Time
3. DOM ( Event Handling) 
4. Some Applications

##### E.   Functions、Arrays & Objects

1. Function
   1. 三角函式
   2. 計算函式
2.  Array
3. Object & Class
4. Random & Noise

##### F.   Data & Data Visualization

1. load text file (text, csv, json)
2. Text processing (Strings)
3. data analysis / parsing
4. Ajax fetch data
5. Applications
   1. weather app (using map)
   2. iss app (using map)

##### G.  Simulation (Nature of Code)

1. Velocity & Forces
2. Spring
3. Particles
4. Autonomous Agents
5. Fractals

##### H.  Image Processing

1. Pixels manipulation

2. Filter

3. Image Convolution

##### I.    Video

1. Playing video

2. Capture video

3. Some Examples of video

##### J.    Audio

1. Playing audio

2. Microphone 

3. Sound Synthesis

4. Some Examples of Audio