---
title: 3D Drawing & WebGL
date: 2020-02-03 22:43:11
tags:
- 3D
- WebGL
- vertex
categories:
- 多媒體程式設計

---

# 3D繪圖&webGL

## 渲染模式 Rendering

開啟 Canvas 有二個選擇，預設 P2D, 若要調用 WEBGL 對於 3D 的支援，那就要設定 WEBGL:

```
createCanvas(800, 600);
createCanvas(800, 600, P2D);  // 同上
createCanvas(800, 600, WEBGL);  // 使用 WEBGL 渲染程式庫
```

WebGL 是 web 上的繪圖加速程式庫，在進行與 3D 相關的渲染都需要有 WebGL 的支援。要留意的是，若我們的 canvas 是 WEBGL, 那麼預設的 (0, 0, 0) 會是在畫面的中央。

註：createGraphics() 也是有同樣的渲染選項。



## Vertex

任意形狀可以由 vertex 組成。基本指令：

### 2D Vertex

- [beginShape()](https://p5js.org/reference/#/p5/beginShape) 建立 實的shape
- [endShape()](https://p5js.org/reference/#/p5/endShape)
- [beginContour()](https://p5js.org/reference/#/p5/beginContour) 建立 虛的shape (挖空)
- [endContour()](https://p5js.org/reference/#/p5/endContour)
- [vertex()](https://p5js.org/reference/#/p5/vertex) 直線連接, 2D
- [bezierVertex()](https://p5js.org/reference/#/p5/bezierVertex) 貝茲線連接
- [curveVertex()](https://p5js.org/reference/#/p5/curveVertex) 區線連接
- [quadraticVertex()](https://p5js.org/reference/#/p5/quadraticVertex) 二次線連接

The parameters available for [beginShape()](https://p5js.org/reference/#/p5/beginShape) are POINTS, LINES, TRIANGLES, TRIANGLE_FAN, TRIANGLE_STRIP, QUADS, and QUAD_STRIP.

雖然有些 shape 不需要使用 WEBGL, 但建議有使用到 vertex ，就使用 WEBGL 渲染。

## Texture

3D Shape 的表面可以貼上影像或是給予計算的色彩來形成表面材料，在P5JS 的 WEBGL 支援了不同的表面材料渲染的功能：

- [texture()](https://p5js.org/reference/#/p5/texture)

- [textureMode()](https://p5js.org/reference/#/p5/textureMode) 分為 NORMAL (正規化到 0 - 1 之間) 以及 IMAGE (實際像素單位)

- [textureWrap()](https://p5js.org/reference/#/p5/textureWrap)

  

## 3D Drawing Primitives

正式進入 3D 繪圖，記得要使用 createCanvas(800, 600, WEBGL); 

### 3D Primitives

- [plane()](https://p5js.org/reference/#/p5/plane) 　平面，用來製作地板
- [box()](https://p5js.org/reference/#/p5/box)
- [sphere()](https://p5js.org/reference/#/p5/sphere)
- [cylinder()](https://p5js.org/reference/#/p5/cylinder)
- [cone()](https://p5js.org/reference/#/p5/cone)
- [ellipsoid()](https://p5js.org/reference/#/p5/ellipsoid)
- [torus()](https://p5js.org/reference/#/p5/torus)　甜甜圈

3D Shape 由三角形構成，圓滑與否由解析度來決定。detailX, detailY 決定二個方向上的解析度。數值愈大，愈圓滑，占用的計算時間也就愈多。

## Lights & Materials

### Interaction

- [orbitControl()](https://p5js.org/reference/#/p5/orbitControl)　讓拖拉滑鼠可以改變3D世界的視角，試著滑鼠按下左鍵上下左右移動。
- [debugMode()](https://p5js.org/reference/#/p5/debugMode)　　顯示3D的座標示意，方便審視你的 3D　物件
- [noDebugMode()](https://p5js.org/reference/#/p5/noDebugMode)　解除 debugMode

### Lights

- [ambientLight()](https://p5js.org/reference/#/p5/ambientLight)　環境光
- [specularColor()](https://p5js.org/reference/#/p5/specularColor)　鏡面反射光
- [directionalLight()](https://p5js.org/reference/#/p5/directionalLight)　方向光
- [pointLight()](https://p5js.org/reference/#/p5/pointLight)　點光
- [lights()](https://p5js.org/reference/#/p5/lights) 一般打光
- [lightFalloff()](https://p5js.org/reference/#/p5/lightFalloff)　光衰減率　由 1/(a + bx + cx^2) 公式來計算，參數三個 a, b, c。　x 是距離。
- [spotLight()](https://p5js.org/reference/#/p5/spotLight)　聚焦光
- [noLights()](https://p5js.org/reference/#/p5/noLights)　停止打光

### Material

3D　物件表面的質感是一個很複雜的演算法，底層可由一個 Shader 來計算。Shader 我們再下一小節介紹，因次這裡只介紹能直接用在 3D 物件上的材質。

https://p5js.org/examples/3d-materials.html　在這個網頁可以看到normal, ambient及specular材質的demo。

- [loadShader()](https://p5js.org/reference/#/p5/loadShader)
- [createShader()](https://p5js.org/reference/#/p5/createShader)
- [shader()](https://p5js.org/reference/#/p5/shader)
- [resetShader()](https://p5js.org/reference/#/p5/resetShader)
- [normalMaterial()](https://p5js.org/reference/#/p5/normalMaterial)　正規材質 (自動著色)
- [ambientMaterial()](https://p5js.org/reference/#/p5/ambientMaterial)　環境材質
- [emissiveMaterial()](https://p5js.org/reference/#/p5/emissiveMaterial)　發光材質
- [specularMaterial()](https://p5js.org/reference/#/p5/specularMaterial)　亮面材質
- [shininess()](https://p5js.org/reference/#/p5/shininess)　閃亮度
- [p5.Geometry](https://p5js.org/reference/#/p5.Geometry)
- [p5.Shader](https://p5js.org/reference/#/p5.Shader)

### Camera

- [camera()](https://p5js.org/reference/#/p5/camera)
- [perspective()](https://p5js.org/reference/#/p5/perspective)
- [ortho()](https://p5js.org/reference/#/p5/ortho)
- [frustum()](https://p5js.org/reference/#/p5/frustum)
- [createCamera()](https://p5js.org/reference/#/p5/createCamera)
- [p5.Camera](https://p5js.org/reference/#/p5.Camera)
- [setCamera()](https://p5js.org/reference/#/p5/setCamera)

## Shader (briefly introduction)



## Transform (in 3D case)

- [applyMatrix()](https://p5js.org/reference/#/p5/applyMatrix)
- [resetMatrix()](https://p5js.org/reference/#/p5/resetMatrix)
- [rotate()](https://p5js.org/reference/#/p5/rotate)　2D 專用
- [rotateX()](https://p5js.org/reference/#/p5/rotateX)　3D 旋轉，X軸方向
- [rotateY()](https://p5js.org/reference/#/p5/rotateY)　3D 旋轉，Ｙ軸方向
- [rotateZ()](https://p5js.org/reference/#/p5/rotateZ)　3D 旋轉，Ｚ軸方向
- [scale()](https://p5js.org/reference/#/p5/scale)　2D, 3D 共用
- [shearX()](https://p5js.org/reference/#/p5/shearX)　2D 專用（水平歪斜)
- [shearY()](https://p5js.org/reference/#/p5/shearY)　2D 專用（垂直歪斜)
- [translate()](https://p5js.org/reference/#/p5/translate)　2D, 3D 共用



------



## Rendering

HTML5 設計了 canvas 來進行繪圖動畫。P5 對應的設計了　p5.Graphics 來渲染出現在 canvas 上的像素。我們的基礎應用都是 createCanvas()　定義一個 canvas。

- [createCanvas()](https://p5js.org/reference/#/p5/createCanvas)　建立一個 Canvas, 我們除了寬高之外，也可指定渲染引擎 WEBGL或P2D。
- [resizeCanvas()](https://p5js.org/reference/#/p5/resizeCanvas)　更改目前 canvas 的寬高
- [noCanvas()](https://p5js.org/reference/#/p5/noCanvas)　去掉預設的 Canvas。通常是我們有其他的應用！
- [p5.Graphics](https://p5js.org/reference/#/p5.Graphics) 可以用來建立一個 off-screen graphics，意思是我們可以建立一個非目前畫布的畫布，在上面進行所有 canvas 可以執行的指令，只是若沒有將這個 graphics 放在我們的網頁上，這個畫布不會被看到。我們可以使用 createGraphics() 指令產生一個 p5.Graphics。
- [createGraphics()](https://p5js.org/reference/#/p5/createGraphics)
- [blendMode()](https://p5js.org/reference/#/p5/blendMode)　設定色彩混合的模式，這會讓一個shape 與另一個shape有重疊時，色彩計算的方式。mode: BLEND, DARKEST, LIGHTEST, DIFFERENCE, MULTIPLY, EXCLUSION, SCREEN, REPLACE, OVERLAY, HARD_LIGHT, SOFT_LIGHT, DODGE, BURN, ADD, REMOVE or SUBTRACT。
- [setAttributes()](https://p5js.org/reference/#/p5/setAttributes)　設定有關渲染的參數。有下列幾個參數：常用反鋸齒來美化畫面，3D使用精緻打光美化漸層。

> alpha - indicates if the canvas contains an alpha buffer default is true。是否有alpha層。
>
> depth - indicates whether the drawing buffer has a depth buffer of at least 16 bits - default is true是否繪圖緩衝區有一個至少16bit的深度緩衝。
>
> stencil - indicates whether the drawing buffer has a stencil buffer of at least 8 bits。模版緩衝（stencil buffer）或印模緩衝，是在OpenGL三維繪圖等計算機圖像硬件中常見的除顏色緩衝、像素緩衝、深度緩衝之外另一種數據緩衝。
>
> antialias - indicates whether or not to perform anti-aliasing default is false。反鋸齒。
>
> premultipliedAlpha - indicates that the page compositor will assume the drawing buffer contains colors with pre-multiplied alpha default is false
>
> preserveDrawingBuffer - if true the buffers will not be cleared and and will preserve their values until cleared or overwritten by author (note that p5 clears automatically on draw loop) default is true　保留繪圖緩衝直到被清掉或改掉。
>
> perPixelLighting - if true, per-pixel lighting will be used in the lighting shader. default is false精緻打光