function setup() {
    createCanvas(400, 400);
    strokeWeight(1);
    stroke(255, 255, 0);
  }
  let N = 100;
  let R = 150;
  let times = 2;
  function draw() {
    background(220);
    N = int(mouseX / width * 200);
    times = int(mouseY / height * 10);
    translate(width/2, height/2);
    for( i = 0; i < N; i++){
      let p = pos(i);
      stroke(255, 0, 0); strokeWeight(5);
      point( p.x, p.y);
      j = i * times;
      let q = pos(j % N);
      stroke(0, 255, 0); strokeWeight(1);
      line( p.x, p.y, q.x, q.y);
    }
    
  }
  function pos(n){
     p = createVector(cos(n/N*TWO_PI), sin(n/N*TWO_PI));
     p.mult(R);
     return p;
  }