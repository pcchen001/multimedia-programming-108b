// 比較 Simplex Noise 與 經典 Noise 可以發覺
// Simplex Noise 更圓滑變化
var h, w;
var zoff = 0;
var simplex;
function setup(){
   w = window.innerWidth;
   h = window.innerHeight;
   createCanvas(w, h);
   simplex = new SimplexNoise();
}

function draw(){
  background(0);
  noFill();

  zoff += 0.01;
  for(let y = 40; y < 80; y+=0.5){
    beginShape();
    colorMode(HSB, 100);
    h = sin(zoff)*50+50;
    stroke(h, 255, 200, 20);
    for(let x = -10; x < innerWidth; x+=5){
      var n = simplex.noise3D(x/100, y/100, zoff);
      var n1 = simplex.noise3D(x/100+1000,y/100+1000, zoff+1000);
      // var n = noise(x/150, y/50, zoff)*2-1;
      //  var n1 = noise(x/150+1000,y/50+1000, zoff+1000)*2-1;
      var y1 = y + map(n, -1, 1, -100, 100) +h/2;  //  調整 200 ~ 100 看效果
       var x1 = x + map(n1, -1, 1, -140, 140);    // 調整 140 ~ 80 看效果
      vertex(x1, y1);
    }
    endShape();
  }
  // console.log(h+".."+w);
}
