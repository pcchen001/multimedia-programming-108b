---
title: simu-NOC3-Osscillation
date: 2020-05-03 21:30:29
tags:
- osscillation
- NOC
categories:
- Nature of Code
- simulation
---

## Oscillation 振盪

這個單元我們處理有角度的 mover, 單擺，彈簧。

### 有角度的 mover

假設一個 mover 是一根棍子，在移動時會旋轉：

```
let angle = 0;
function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220);
  translate(width/2, height/2);
  rotate(angle);
  line(-20, 0, 20, 0);
  ellipse(-20, 0, 5, 5);
  ellipse(20, 0, 5, 5);
  angle += 0.01;
}
```

根據牛頓運動定律，旋轉的運動也是靜者恆靜，動者恆動。

角度，角速度，角加速度。 angle, anVe, anAc

```
let angle = 0;
let anVe = 0; 
let anAc = 0.001;
function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220);
  translate(width/2, height/2);
  anVe += anAc;
  angle += anVe;
  rotate(angle);
  line(-20, 0, 20, 0);
  ellipse(-20, 0, 5, 5);
  ellipse(20, 0, 5, 5);
}
```

Mover 再進化：

```js
class Mover {
  constructor(){
     this.po = createVector(width/2, height/2);
     this.ve = createVector(0, 0);
     this.ac = createVector(0, 0);
     this.m = 1;
     this.angle = 0;
     this.ave = 0;
     this.aac = 0;
  }
  update(){
      this.ve.add(this.ac);
      this.po.add(this.ve);
      this.ave += this.aac;
      this.angle += this.ave;
      this.ac.mult(0);
  }
  display(){
     stroke(0);
     fill(175, 200);
     rectMode(CENTER);
     push();
     translate(this.po.x, this.po.y);
     rotate(this.angle);
      line(-20, 0, 20, 0);
      ellipse(-20, 0, 5, 5);
      ellipse(20, 0, 5, 5);
     pop();
  }
}
```

現在，我們可以試試看讓這個 mover 移動，碰撞，反彈。

### 極座標

```js
let r;
let theta;

function setup() {
  createCanvas(640, 360);
  // Initialize all values
  r = height * 0.45;
  theta = 0;
}

function draw() {
  background(51);

  // Translate the origin point to the center of the screen
  translate(width / 2, height / 2);

  // Convert polar to cartesian
  let x = r * cos(theta);
  let y = r * sin(theta);

  // Draw the ellipse at the cartesian coordinate
  ellipseMode(CENTER);
  fill(127);
  stroke(255);
  strokeWeight(2);
  line(0, 0, x, y);
  ellipse(x, y, 48, 48);

  // Increase the angle over time
  theta += 0.02;
}
```

請試試看，繪出一個螺旋圖形。

### 簡諧運動

```
  let period = 120;
  let amplitude = 300;

  // Calculating horizontal position according to formula for simple harmonic motion
  let x = amplitude * sin(TWO_PI * frameCount / period);
```

另一種寫法： aVelocity = 0.03

```
  let amplitude = 300;
  let x = amplitude * sin(angle);
  angle += aVelocity;
```

圓型運動的投影。

## 單擺

假設單擺掛在 origin 的地方，長度是 r, 假設擺錘到 origin 的直線與垂直線夾角是 angle ，那麼這個擺錘受到的重力場的加速度會是 gravity * sin( angle )。

當擺錘受到一個與擺動方向相同的加速度為 acc 的力的作用時，對應到角加速度會是 acc / r 。

也可以說，若一個半徑是 r 的圓周上，切線方向的移動是 d, 那麼對應的弧度就是  d / r 。速度如果是 v 那對應的角速度就是 v / r 。加速度是 a 那對應的角加速度就是 a / r 。

現在我們來寫單擺的 class

```
class Pendulum{
   constructor(x, y, r){
       this.origin = createVector(x, y); // 掛點
       this.position = createVector();
       this.r = r; // 擺長
       this.aacc = 0;
       this.avel = 0;
       this.angle = PI/4;  //  一開始的位置 45度
   }
   update(){
     let gravity = 0.4;
     let f = gravity * sin( this.angle );
     this.aacc = -f / this.r;  // 順時鐘為正，逆時鐘為負。
     this.avel += this.aacc;
     this.angle += this.avel;
   }
   display(){
      let mx = this.r * cos(this.angle-PI/2)
      let my = -this.r * sin(this.angle-PI/2)
      this.position.set(mx, my)
      this.position.add(this.origin)
      ellipse(this.position.x, this.position.y, 50, 50)
   }
}
let p;
function setup() {
  createCanvas(400, 600);
  p = new Pendulum(width/2, 0, 200);
}

function draw() {
  background(220);
  p.update();
  p.display(); 
}
```

我們可以加上一點互動，讓模擬更有趣。

```
class Pendulum{
   constructor(x, y, r){
       this.origin = createVector(x, y); // 掛點
       this.position = createVector();
       this.r = r; // 擺長
       this.aacc = 0;
       this.avel = 0;
       this.angle = PI/4;  //  一開始的位置 45度
       this.dragging = false    // 加上這個拖拉狀態
   }
   update(){
     let gravity = 0.4;
     let f = gravity * sin( this.angle );
     this.aacc = -f / this.r;  // 順時鐘為正，逆時鐘為負。
     this.avel += this.aacc;
     this.angle += this.avel;
   }
   display(){
      let mx = this.r * cos(this.angle-PI/2)
      let my = -this.r * sin(this.angle-PI/2)
      this.position.set(mx, my)
      this.position.add(this.origin)
      line(this.origin.x, this.origin.y, this.position.x, this.position.y) // 加一條線
      ellipse(this.position.x, this.position.y, 50, 50)
      
   }
     clicked(mx, my){
        let d = dist(mx, my, this.position.x, this.position.y);
    if( d < 50){
        this.dragging = true
    }
  }
  stopDragging(){
     this.dragging = false;
     this.avel = 0;
  }
  drag(){
    if(this.dragging){
      let diff=p5.Vector.sub( createVector(mouseX, mouseY), this.origin,);
      this.angle = -diff.heading()+PI/2;
    }
  }
}
let p;
function setup() {
  createCanvas(400, 600);
  p = new Pendulum(width/2, 0, 200);
}

function draw() {
  background(220);
  p.update();
  p.drag();
  p.display(); 
}
function mousePressed(){
  p.clicked(mouseX, mouseY);
}
function mouseReleased(){
  p.stopDragging();
}
```

如果要模擬有摩擦力的單擺，在擺了足夠多次之後會停下來，那就可以在 每次的 角速度變化時，乘上阻尼係數，例如 0.998 。

## 彈簧

假設我們單擺掛上去的是一個彈簧，那麼我們的擺錘就會受到彈簧的影響。

一個彈簧的模擬是一個長度，當長度改變時會引發虎克定律的彈力：彈力與形變大小成正比。

若拉長 dl, 那麼就會有向掛點的 k * dl 的作用力。縮短 dl 那麼就會有離開掛點的 k*dl 的作用力。

假設掛點是 origin, 原長度是  len, 錘頭的位置是 position, 那麼作用在 錘頭上的力是

```
let f = p5.Vector.sub(origin, position)
let dl = f.mag() - len
f.normalize().mult(-k*dl)
```

寫 class

```
class spring{
  constructor(x, y, len){
     this.anchor = createVector(x, y)
     this.len = len
     this.k = 0.2    // 彈性係數
  }
  connect(bob){
    let f = p5.Vector.sub(bob.position, this.anchor)
    let dl = f.mag() - this.len
    f.normalize().mult(-this.k*dl) 
    bob.applyForce(f)
  }
  display(bob){
    line(this.anchor.x, this.anchor.y, bob.position.x, bob.position.y)
  }
```

寫 鉛錘 class, 回想我們的 mover 

```
class Bob{
  constructor(x, y){
    this.position=createVector(x, y)
    this.vel=createVector()
    this.acc=createVector()
    this.mass = 24;
  }
  update(){
    this.vel.add(this.acc)
    this.position.add(this.vel)
    this.acc.mult(0)
  }
  applyForce(force){
    let f = force.copy()
    f.div(this.mass)
    this.acc.add(f)
  }
  display(){
    ellipse(this.position,x, this.position.y, 50, 50)
  }
}
```

sketch.js

```
let s, p;
  
function setup() {
  createCanvas(400, 600);
  p = new Bob(width/2+50, 220);
  s = new Spring(width/2, 0, 200);
  s.connect(p)
}

function draw() {
  background(220);
  p.update();
  s.connect(p)
  s.display(p);
  p.display(); 
}
function mousePressed(){
  p.clicked(mouseX, mouseY);
}
function mouseReleased(){
  p.stopDragging();
}
```

現在，我們加上重力場以及一點阻尼

```
class Bob{
  constructor(x, y){
    this.position=createVector(x, y)
    this.vel=createVector()
    this.acc=createVector()
    this.mass = 24;
  }
  update(){
    this.vel.add(this.acc)
    this.vel.mult(0.99)     // 阻尼
    this.position.add(this.vel)
    this.acc.mult(0)
  }
  ...
function draw() {
  background(220);
  let gravity = createVector(0, 2);  // 重力場
  p.applyForce(gravity);
  p.update();
  s.connect(p)
  p.display(); 
  s.display(p);
}
 
```

最後，加上拖拉功能！  自己做囉！

如果一個彈簧掛上二個鉛錘，自由移動：

```
class Spring{
  constructor(a, b, len){
    this.a = a;
    this.b = b;
    this.len = len;
    this.k = 0.2
  }
  update(){
    let f = p5.Vector.sub(this.a.position, this.b.position)
    let d = f.mag()
    let dlen = d - this.len
    f.normalize().mult(-this.k*dlen)
    this.a.applyForce(f)
    f.mult(-1)
    this.b.applyForce(f)
  }
  display(){
     line(this.a.position.x, this.a.position.y,
          this.b.position.x, this.b.position.y);
  }
}
```

鉛錘不用更動，setup 及 draw

```
let s, p, q;
  
function setup() {
  createCanvas(400, 600);
  p = new Bob(width/2+50, 220);
  q = new Bob(width/2-50, 100);
  s = new Spring(p, q, 200);
}

function draw() {
  background(220);
  let gravity = createVector(0, 2);
  p.applyForce(gravity);
  q.applyForce(gravity);
  p.update(); q.update();
  s.update();
  p.display(); q.display();
  s.display();
}
```

我們應該可以觀察到彈簧的現象，只是因為沒有準備邊沿偵測，所以...

請同學修改一下程式：

1. 拿掉重力場
2. 二個彈簧掛三個鉛錘
3. 加上拖拉功能
4. 加上重力場

