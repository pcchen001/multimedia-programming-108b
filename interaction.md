---
title: Interaction and DOM
date: 2020-04-7 15:00:00
tags:
- Interaction
- DOM
- Mobile Device
categories:
- Basics
---

P5js 將 HTML 5 DOM 做了一些包裝，好讓程式比較親切一些。不熟 DOM 的同學可以不用理會 DOM, 就當作新的輸入方式來學習就好。

最基本的輸入包含滑鼠及鍵盤：

## 滑鼠

### Mouse

滑鼠座標及狀態

- [movedX](https://p5js.org/reference/#/p5/movedX) x 方向移動量
- [movedY](https://p5js.org/reference/#/p5/movedY)
- [mouseX](https://p5js.org/reference/#/p5/mouseX)  在Canvas 的位置
- [mouseY](https://p5js.org/reference/#/p5/mouseY)
- [pmouseX](https://p5js.org/reference/#/p5/pmouseX) 前一個位置
- [pmouseY](https://p5js.org/reference/#/p5/pmouseY)
- [winMouseX](https://p5js.org/reference/#/p5/winMouseX) 在視窗中的座標
- [winMouseY](https://p5js.org/reference/#/p5/winMouseY)
- [pwinMouseX](https://p5js.org/reference/#/p5/pwinMouseX) 前一個在視窗中的座標
- [pwinMouseY](https://p5js.org/reference/#/p5/pwinMouseY)
- [mouseButton](https://p5js.org/reference/#/p5/mouseButton) LEFT, RIGHT, CENTER
- [mouseIsPressed](https://p5js.org/reference/#/p5/mouseIsPressed) true/false

定義滑鼠事件處理函式：

- [mouseMoved()](https://p5js.org/reference/#/p5/mouseMoved) / touchMoved() 
- [mouseDragged()](https://p5js.org/reference/#/p5/mouseDragged) / touchMoved()
- [mousePressed()](https://p5js.org/reference/#/p5/mousePressed) / touchStart()
- [mouseReleased()](https://p5js.org/reference/#/p5/mouseReleased) / touchEnded()
- [mouseClicked()](https://p5js.org/reference/#/p5/mouseClicked)
- [doubleClicked()](https://p5js.org/reference/#/p5/doubleClicked)
- [mouseWheel()](https://p5js.org/reference/#/p5/mouseWheel)  mouseWheel(event){ p+= event.delta } // 滾動量

游標顯隱控制：

- [requestPointerLock()](https://p5js.org/reference/#/p5/requestPointerLock) 隱藏游標
- [exitPointerLock()](https://p5js.org/reference/#/p5/exitPointerLock) 停止隱藏游標

練習：滾動畫布

## 鍵盤

- [keyIsPressed](https://p5js.org/reference/#/p5/keyIsPressed)
- [key](https://p5js.org/reference/#/p5/key)
- [keyCode](https://p5js.org/reference/#/p5/keyCode)

鍵盤事件處理函式：

- [keyPressed()](https://p5js.org/reference/#/p5/keyPressed) keyCode
- [keyReleased()](https://p5js.org/reference/#/p5/keyReleased) keyCode
- [keyTyped()](https://p5js.org/reference/#/p5/keyTyped)  key
  - key 一般按鍵 取得ascii 碼 
  - keyCode 特殊按鍵BACKSPACE, DELETE, ENTER, RETURN, TAB, ESCAPE, SHIFT, CONTROL, OPTION, ALT, UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW。

查詢按鍵情況

- [keyIsDown()](https://p5js.org/reference/#/p5/keyIsDown) keyIsDown(code) return true/false。 
  - code: BACKSPACE, DELETE, ENTER, RETURN, TAB, ESCAPE, SHIFT, CONTROL, OPTION, ALT, UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW。或是 ascii 碼。

上下左右控制飛行器、乒乓球、...

## 行動裝置的觸控 Touch

- [touches](https://p5js.org/reference/#/p5/touches) 位置的陣列 .x 以及 .y touches.length, for( p in touches ){ p.x,   p.y}
- [touchStarted()](https://p5js.org/reference/#/p5/touchStarted)
- [touchMoved()](https://p5js.org/reference/#/p5/touchMoved)
- [touchEnded()](https://p5js.org/reference/#/p5/touchEnded)

**注意**：這個單元的練習需要行動裝置，你必須將程式放到伺服器端，再將行動裝置連結到網址去測試。

## 行動裝置的三軸加速器

### Acceleration

- [deviceOrientation](https://p5js.org/reference/#/p5/deviceOrientation) LANDSCAPE or PORTRAIT。
- [accelerationX](https://p5js.org/reference/#/p5/accelerationX)
- [accelerationY](https://p5js.org/reference/#/p5/accelerationY)
- [accelerationZ](https://p5js.org/reference/#/p5/accelerationZ)
- [pAccelerationX](https://p5js.org/reference/#/p5/pAccelerationX)
- [pAccelerationY](https://p5js.org/reference/#/p5/pAccelerationY)
- [pAccelerationZ](https://p5js.org/reference/#/p5/pAccelerationZ)
- [rotationX](https://p5js.org/reference/#/p5/rotationX)
- [rotationY](https://p5js.org/reference/#/p5/rotationY)
- [rotationZ](https://p5js.org/reference/#/p5/rotationZ)
- [pRotationX](https://p5js.org/reference/#/p5/pRotationX)
- [pRotationY](https://p5js.org/reference/#/p5/pRotationY)
- [pRotationZ](https://p5js.org/reference/#/p5/pRotationZ)

------

裝置旋轉時會觸發 deviceTurned() 進行事件處理，此時若 turnAxis == 'X' 表示 X 軸旋轉， turnAxis=='Y' 表示 Y 軸旋轉，turnAxis=='Z' 表示 Z軸旋轉。

偵測裝置移動，可以 setMoveThreshold( fhreshold ) 設定觸發 deviceMoved() 事件處理的門閥值，值愈大愈不容易觸發。 預設為 0.5 。

偵測裝置搖到，可以 setShakeThreshold( threshold ) 設定觸發 deviceShaken() 事件處理的門閥值，值愈大愈不容易觸發。預設為 30 。

- [turnAxis](https://p5js.org/reference/#/p5/turnAxis)
- [setMoveThreshold()](https://p5js.org/reference/#/p5/setMoveThreshold)
- [setShakeThreshold()](https://p5js.org/reference/#/p5/setShakeThreshold)
- [deviceMoved()](https://p5js.org/reference/#/p5/deviceMoved)
- [deviceTurned()](https://p5js.org/reference/#/p5/deviceTurned)
- [deviceShaken()](https://p5js.org/reference/#/p5/deviceShaken)

**注意**：這個單元的練習需要行動裝置，你必須將程式放到伺服器端，再將行動裝置連結到網址去測試。

### Time & Date

- [hour()](https://p5js.org/reference/#/p5/hour) 、[minute()](https://p5js.org/reference/#/p5/minute) 、[second()](https://p5js.org/reference/#/p5/second)
- [day()](https://p5js.org/reference/#/p5/day) 、[month()](https://p5js.org/reference/#/p5/month) 、[year()](https://p5js.org/reference/#/p5/year)
- [millis()](https://p5js.org/reference/#/p5/millis) 程式執行時起算， ms 為單位
- 練習：
  1. 時鐘
  2. 計時器



HTML 5 提供的輸入介面， P5js 做了支援。

p5.Element 是HTML5 DOM 的元素物件類別。 P5js 提供了方便且風格與 P5js 類似的指令。熟悉 JS 的同學使用 JS 指令也是可以的。

## DOM

- [p5.Element](https://p5js.org/reference/#/p5.Element)

- 選擇器，用來對應網頁上的元素，# 對應 id,  . 對應 class 沒有 # 或是 . 則是 tagname 。第二個參數是 container 選擇器，可用來選擇某個元素內的元素

  - [select()](https://p5js.org/reference/#/p5/select) # 對應 id,  . 對應 class 沒有 # 或是 . 則是 tagname 。第二個參數是 container 選擇器
  - [selectAll()](https://p5js.org/reference/#/p5/selectAll) 返回陣列。 . 對應 class 沒有 . 則是 tagname 。第二個參數是 container 選擇器

- 創建元素

  - [createDiv()](https://p5js.org/reference/#/p5/createDiv)
  - [createP()](https://p5js.org/reference/#/p5/createP)
  - [createSpan()](https://p5js.org/reference/#/p5/createSpan)
  - [createImg()](https://p5js.org/reference/#/p5/createImg)，img 元素，給url, alt 。另外也可以設定 CORS 以及 callback Function。CORS 設定值有三種："anonymous", "use-credentials" 以及 ""。空字串表示不使用 CORS。
  - [createA()](https://p5js.org/reference/#/p5/createA)，超連結， url, html，可以指定 target: _blank, _self, _parent, _top。
  - [createElement()](https://p5js.org/reference/#/p5/createElement)，其他類的元素 e.g. createElement('h2', "標題2號")

- 輸入元素的創建、事件處理及刪除

  - [createSlider()](https://p5js.org/reference/#/p5/createSlider) 滑桿

  - [createButton()](https://p5js.org/reference/#/p5/createButton) 按鈕

  - [createCheckbox()](https://p5js.org/reference/#/p5/createCheckbox) 檢核格

  - [createSelect()](https://p5js.org/reference/#/p5/createSelect) 選單

  - [createRadio()](https://p5js.org/reference/#/p5/createRadio) 圓紐

  - [createColorPicker()](https://p5js.org/reference/#/p5/createColorPicker) 色彩

  - [createInput()](https://p5js.org/reference/#/p5/createInput) 文字輸入，可以是 text 或是 password。 例如： inp = createInput("預設","password")。

    - [input()](https://p5js.org/reference/#/p5/input) 為所建立的文字輸入元素設定 callback function。例如： inp.input( callbackFunction )。
    - [changed()](https://p5js.org/reference/#/p5/changed) 設定元素發生更動時的事件處理

  - [createFileInput()](https://p5js.org/reference/#/p5/createFileInput) 檔案選擇，需要 callback, 所選的檔案會被當作引數傳給 callback。

    - [p5.File](https://p5js.org/reference/#/p5.File)

    ```js
    let img;
    
    function setup() {
      input = createFileInput(handleFile);
      input.position(0, 0);
    }
    
    function draw() {
      background(255);
      if (img) {
        image(img, 0, 0, width, height);
      }
    }
    
    function handleFile(file) {
      print(file);
      if (file.type === 'image') {
        img = createImg(file.data, '');
        img.hide();
      } else {
        img = null;
      }
    }
    ```

    拖放一個影像檔案，p5 有很簡便的寫法：

    ```js
    let img;
    
    function setup() {
      let c = createCanvas(100, 100);
      background(200);
      textAlign(CENTER);
      text('drop image', width / 2, height / 2);
      c.drop(gotFile);
    }
    
    function draw() {
      if (img) {
        image(img, 0, 0, width, height);
      }
    }
    
    function gotFile(file) {
      img = createImg(file.data, '').hide();
    }
    ```

    Element.drop(callback) 是每一個元素都能有的 drop事件處理設定。

    其他的事件處理設定包括：

    1. mousePressed(), doubleClicked(), mouseWheel(), mouseReleased(), mouseClicked(), moseMoved(), mouseOver(), mouseOut()
    2. touchStarted(), touchMoved(), touchEnded(), 
    3. dragOver(), dragLeave()
    4. drop()

  - [removeElements()](https://p5js.org/reference/#/p5/removeElements) 刪除元素

  - DOM 元素常用方法：[p5.Element](https://p5js.org/reference/#/p5.Element)

    - parent()　接到某個元素

    - child() 納入某個元素作為 child 

    - id(), 設定或讀取Element的 id

    - class(), 設定或讀取Element的 classes

    - addClass(), removeClass(), hasClass(), toggleClass() 更動部分 class

    - html() 設定會讀取 Element的 innerHTML

    - position(x, y) 設定在 Canvas 的 x, y 位置

    - style() 設定或讀取 style。　button.style('background-color', 'lightgreen'), v = button.style('width')

    - attribute()　設定或讀取 attribute

    - value() 元素的 value 屬性值

    - show(), hide() 　display:block / display:none

    - size() 　設定寬、高或讀取寬高　el.size(200, 100)   el.size().width, el.size().height 。一般直接使用 el.width, wl.height 也可以。

      

- 影音、聲音輸出入

  - [createVideo()](https://p5js.org/reference/#/p5/createVideo)
  - [createAudio()](https://p5js.org/reference/#/p5/createAudio)
  - [createCapture()](https://p5js.org/reference/#/p5/createCapture) 攝影鏡頭、麥克風
  - [VIDEO](https://p5js.org/reference/#/p5/VIDEO)
  - [AUDIO](https://p5js.org/reference/#/p5/AUDIO)
  - [p5.MediaElement](https://p5js.org/reference/#/p5.MediaElement) 這是 影音、聲音輸出入的元素，共同提供播放、控制、... 等功能指令。



