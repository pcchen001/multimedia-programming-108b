---
title: Function, Array and Object
date: 2020-04-14 15:00:40
tags:
- Function
- Array
- Class
- Object
- Random
- Noise
- Recursion
categories:
- Basics
---

本單元介紹 JS 的程式語言基礎結構，包含 Function, Array, Object 以及 Class。

除了基礎的概念外，我們要介紹如何設計 JS 的類別來方便物件導向程式設計。

我們也要介紹遞迴程式與碎形設計的關聯，這是數值影像的重要核心。

亂數及雜訊函式則是用來模擬自然形象或是新創設計的好用工具。

## Functions

P5js 基於 JS. 函式語法遵循 JS 的規則。

函式定義：

```js
function functionName( par1, par2){
   statement1;
   statement2;
   return something;
}
// 下面寫法也可以
var fname = function(par1, par2){
    statement1;
    statement2;
    return somethingOrNot;
}
```

P5js 改寫或支援的函式包含

### Trigonometry 三角函式

- [acos()](https://p5js.org/reference/#/p5/acos) 、[asin()](https://p5js.org/reference/#/p5/asin) 、[atan()](https://p5js.org/reference/#/p5/atan) 三種三角函式的反函式。
- [atan2()](https://p5js.org/reference/#/p5/atan2) 計算角度用的反正切函式
- [cos()](https://p5js.org/reference/#/p5/cos) 正弦
- [sin()](https://p5js.org/reference/#/p5/sin) 餘弦
- [tan()](https://p5js.org/reference/#/p5/tan) 正切
- [degrees()](https://p5js.org/reference/#/p5/degrees) 轉角度
- [radians()](https://p5js.org/reference/#/p5/radians) 轉徑度
- [angleMode()](https://p5js.org/reference/#/p5/angleMode) 角度模式 預設徑度，可改為角度。

### Calculation 常用計算

- [abs()](https://p5js.org/reference/#/p5/abs) 取絕對值
- [ceil()](https://p5js.org/reference/#/p5/ceil) 、[floor()](https://p5js.org/reference/#/p5/floor) 取整數 [round()](https://p5js.org/reference/#/p5/round) 四捨五入
- [fract()](https://p5js.org/reference/#/p5/fract) 取小數部分
- [dist()](https://p5js.org/reference/#/p5/dist) 二點距離
- [exp()](https://p5js.org/reference/#/p5/exp) 、[pow()](https://p5js.org/reference/#/p5/pow) 指數
- [log()](https://p5js.org/reference/#/p5/log) 對數
- [lerp()](https://p5js.org/reference/#/p5/lerp) 內插 、[map()](https://p5js.org/reference/#/p5/map) 對應、[norm()](https://p5js.org/reference/#/p5/norm) 正規化、[constrain()](https://p5js.org/reference/#/p5/constrain) 限制
- [mag()](https://p5js.org/reference/#/p5/mag) 向量長度，平方和開根號
- [max()](https://p5js.org/reference/#/p5/max) 陣列或二個數的最大
- [min()](https://p5js.org/reference/#/p5/min)陣列或二個數的最小
- [sq()](https://p5js.org/reference/#/p5/sq) 平方
- [sqrt()](https://p5js.org/reference/#/p5/sqrt) 平方根

## Array 陣列

JS 的陣列基本語法：範例

```
jarr = [];
jarr[1] = 100;
jarr['a'] = 200;
jarr.push(e);
jarr.pop();

for( let x of jarr)
    console.log(x);
```

JS 提供了不少好用的陣列函式，P5js也有提供，但未來會被移除。因此我們介紹　 [JS Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

- array.push(v)　新增

- array.pop()　刪除最後一個

- array.concat()　連接另一個陣列

- array.reverse()

- array.sort()

- array.slice()　部分複製

  - 複製一個陣列

    ```js
    let shallowCopy = fruits.slice() // this is how to make a copy
    ```

    取出部分陣列

    ```js
    let shallowCopy = fruits.slice(begin, end) // this is how to make a copy
    所取出的部分陣列為　[begin, end), e.g. (2, 5) 第 2, 3, 4 共三個。
    ```

- array.splice() 　部分移除/取代

  - 移除第 pos 個, 

    ```js
    let removedItem = fruits.splice(pos, 1) // this is how to remove an item
    ```

  - 移除 第 pos起的 n 個

    ```js
    let removedItems = vegetables.splice(pos, n)
    ```

- [shuffle()](https://p5js.org/reference/#/p5/shuffle) 

## Object & Class

JS 資料物件為 JSON ，是一種遞迴結構的表示

```
{
  'key': value,
  'key': value
}
```

key 只能是字串或是整數，value 除了基本型別外，也可以是陣列或是JSON。

JSON 是目前最廣泛使用的資料表是格式，還不熟悉的同學盡快認識它。



JS 一般物件非常通用，函式通常也可以視為一個物件。

JS 的類別定義，基本範例如下：

```js
class Mover {
  constructor(){
    this.position = createVector(random(width), random(height));
    this.velocity = createVector(random(-2, 2), random(-2, 2));
  }

  update() {
    this.position.add(this.velocity);
  }

  display() {
    stroke(0);
    strokeWeight(2);
    fill(127);
    ellipse(this.position.x, this.position.y, 48, 48);
  }

  checkEdges() {

    if (this.position.x > width) {
      this.position.x = 0;
    }
    else if (this.position.x < 0) {
      this.position.x = width;
    }

    if (this.position.y > height) {
      this.position.y = 0;
    }
    else if (this.position.y < 0) {
      this.position.y = height;
    }
  }
}
```

創建物件：

```js
let mover;

function setup() {
  createCanvas(640,360);
  mover = new Mover(); 
}

function draw() {
  background(51);

  mover.update();
  mover.checkEdges();
  mover.display();
}
```

一個陣列的物件：

```js
let movers=[];

function setup() {
  createCanvas(640,360);
  for(let i = 0; i < 10; i++)
    movers.push(new Mover()); 
}

function draw() {
  background(51);
  for(mover of movers){
      mover.update();
      mover.checkEdges();
      mover.display();      
  }
}
```

JS 的類別，可以之後再增加成員函式或是資料成員：

```js
Mover.prototype.applyForce = function(force) {
    let f = p5.Vector.div(force, this.mass);
    this.acceleration.add(f);
  }

Mover.prototype.acceleration = createVector(0,0);
```

## Random and Noise

P5JS 提供基本的 Random 及 Noise 函式：

### Random

- [randomSeed()](https://p5js.org/reference/#/p5/randomSeed)
- [random()](https://p5js.org/reference/#/p5/random)　根據所給數值產生平均分配的隨機數值
- [randomGaussian()](https://p5js.org/reference/#/p5/randomGaussian)　提供以高斯分配所產生的隨機數值
- 練習
  - 視覺化 random 的一維分配
  - 視覺化 random 的二維分配
  - 視覺化 randomGaussian 的一維分配
  - 大小圈圈填空隙

### Noise

- [noise()](https://p5js.org/reference/#/p5/noise)
- [noiseDetail()](https://p5js.org/reference/#/p5/noiseDetail)
- [noiseSeed()](https://p5js.org/reference/#/p5/noiseSeed)
- 練習
  - 股市曲線模擬　一維
  - 蝴蝶產生器　二維
  - 地形產生器　二維
  - 極光產生器　三維

### Simplex Noise

simplex noise(2001) 是 傳統 perlin noise (1983) 的改良版。高維計算上， simplex 有更好的效果及效率。

不少程式庫都提供了開源，我們使用下列的程式庫就可以了！

```js
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplex-noise/2.4.0/simplex-noise.min.js">
```

[Simplex 網站](https://github.com/jwagner/simplex-noise.js?files=1)

範例：　plasma

```js
var h, w;
var zoff = 0;
var simplex;
var scal;
function setup(){
   w = 300;
   h = 300;
   createCanvas(300, 300);
   simplex = new SimplexNoise();
   scal = 4;
   noStroke();
}

function draw(){
   background(0);
   var xoff = 0;
   var yoff = 0;
   for(let y = 0; y < h; y+=scal){
     xoff = 0;
     for( let x = 0; x < w; x+=scal){
       var z = simplex.noise3D(xoff, yoff, zoff)*125+125;
       fill(z);
       rect(x, y,scal, scal);
       xoff += 0.1;
     }
     yoff += 0.1;
   }
   zoff += 0.1;
}
```

simplex noise 的使用語法：

```
var simplex = new SimplexNoise(),
    value2d = simplex.noise2D(x, y),
    value3d = simplex.noise3D(x, y, z),
    value4d = simplex.noise4D(x, y, z, w);
```

若要給 SimplexNoise seed ，準備一個字串，例如 'myseed'　作為建構子的引數。

```
var simplex = new SimplexNOise('myseed');
```

