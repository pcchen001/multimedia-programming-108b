---
title: 延伸參考影片及程式碼
date: 2020-06-06 11:52:39
tags:
- challenge
- algorithm
categories:
- Application
---



## The Coding Train ([Coding Challenges](https://thecodingtrain.com/CodingChallenges/))

部分是 processing (java) 的程式碼，不過大部分都有/是 P5js 。

#145 2D RayCasting 光線投射演算法   [Youtube:](https://www.youtube.com/watch?v=TOEi6T2mtHo&t=1025s)

#146 3D RayCasting 3D 光線投射演算法 [3DRaycasting Youtube](https://www.youtube.com/watch?v=vYgIKn7iDH8) 

#93 雙重單擺　順便學習 offscreen 的使用

#60 蝴蝶產生器 noise 應用

#50.1 Circle Packing 圈圈組字（圖） 

#51.1, .2 .3  A* Pathfinding 演算法： A* 是很古典的人工智慧演算法，用來找近似最佳解。

#29 Smart Rocket 會學習的飛彈

##11 3D Terrain Genrerator 地形產生器

#1 Star field 星球場