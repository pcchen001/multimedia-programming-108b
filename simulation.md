---
title: simulation- NOC2
date: 2020-02-23 20:48:09
tags:
- simulation
- physics
categories:
- NOC
---

# Nature of Code

Introduction (Random & Noise)

## [Ch.1 Vectors](https://natureofcode.com/book/chapter-1-vectors/) 

## Ch.2 Forces

牛頓三大運動定律，第一條：

<div align="center" style="background-color: lightgrey"><h3>物體靜者恆靜，動者恆動</h3></div>

沒有受到外力的情況下，不動的就永遠靜止，移動的將依照目前的速度等速度運動。沒有受到外力包含所受到的外力剛好處於平衡狀態，此時受到的外力總和是0。

牛頓三大運動定律，第三條：

<div align="center" style="background-color: lightgrey"><h3>每一個作用力都有一個反作用力，大小相同方向相反。</h3></div>

在計算物體碰撞時，若A 受到B給了 f 的力，那麼 B 也受到 A 大小為 f 方向相反的力。用 P5.Vector 的寫法，f1 = P5.Vector.mult(f, -1)。

牛頓三大運動定律，第二條：

<div align="center" style="background-color: lightgrey"><h3>力 = 質量 X 加速度。</h3></div>

我們可以模擬一個物體的質量為 m，那麼到 f 的力時，物體就會有了 a = f / m 的加速度。若加速度為 a ，那麼速度 v 在一個單位時間內會增加了 a, 也就是 v.add( a )。而位置 p 在一個單位時間內，增加 v ，也就是 p.add( v )。

要模擬一個物理世界的物體，我們就可以設計一個 Mover 類別：

```js
class Mover{
  constructor(p, v){
      this.po = createVector(p.x, p.y);
      this.ve = createVector(v.x, v.y);
      this.ac = createVector(0, 0);
      this.m = 1;
  }
  applyForce(f){
      let a = p5.Vector.div(force, this.mass);
      this.ac.add(a)
  }
  update(){
    this.ve.add(this.ac);
    this.po.add(this.ve);
    this.ac.mult(0);  // 一次受力，產生一次的加速度
  }
  display() {
    stroke(0);
    strokeWeight(2);
    fill(255, 127);
    ellipse(this.po.x, this.po.y, 48, 48);
  }
  checkEdges() {
    if (this.po.x > width) {
      this.po.x = width;
      this.ve.x *= -1;
    } else if (this.po.x < 0) {
      this.ve.x *= -1;
      this.po.x = 0;
    }
    if (this.po.y > height) {
      this.ve.y *= -1;
      this.po.y = height;
    }
  }  
}
```

當物體受到一個外力時，這個外力會依照質量轉換成加速度，對應這個我們可以寫成 applyForce()。然後每個單位時間的變動寫在 update()中。

我們再增加 display() 及 checkEdges()。這樣就完成了符合牛頓三大運動定律的物體。

放到畫面上，我們基礎上可以放上重力，也可以加上一些風力。

```js
let mover;

function setup() {
  createCanvas(640, 360);
  let p = createVector(10, 10);
  let v = createVector(0, 0);
  mover = new Mover(p, v);
  createP('畫布上左鍵滑鼠，啟動一個風力！');
}

function draw() {
  background(51);

  let gravity = createVector(0, 0.1);
  mover.applyForce(gravity);

  if (mouseIsPressed) {
    let wind = createVector(0.1, 0);
    mover.applyForce(wind);
  }

  mover.update();
  mover.display();
  mover.checkEdges();
}
```

記得，在 index.html 中，要加入這二個 .js 。

我們也可以試試看，給一個固定的向右吹得風力，看看模擬的情況：

```js
  let wind = createVector(0.01, 0);
  mover.applyForce(wind);
```

假設我們要有不同質量的物體，我們的類別要做一點修正，建構子要增加一個參數：

```js
class Mover{
  constructor(p, v, m){
      this.po = createVector(p.x, p.y);
      this.ve = createVector(v.x, v.y);
      this.ac = createVector(0, 0);
      this.m = m;
      this.rad = m *8;
  }
  applyForce(f){
      let a = p5.Vector.div(force, this.mass);
      this.ac.add(a)
  }
  update(){
    this.ve.add(this.ac);
    this.po.add(this.ve);
    this.ac.mult(0);  // 一次受力，產生一次的加速度
  }
  display() {
    stroke(0);
    strokeWeight(2);
    fill(255, 127);
    ellipse(this.po.x, this.po.y, this.rad*2, this.rad*2);
  }
  checkEdges() {
    if (this.po.x > width-this.rad) {
      this.po.x = width-this.rad;
      this.ve.x *= -1;
    } else if (this.po.x < this.rad) {
      this.ve.x *= -1;
      this.po.x = 0;
    }
    if (this.po.y > height-this.rad) {
      this.ve.y *= -1;
      this.po.y = height-this.rad;
    }
  }  
}
```

sketch.js 也要有所變動：假設我們模擬 6 個物體：

```js
let movers=[];

function setup() {
  createCanvas(640, 360);
  for( let i = 0; i < 6; i++){
    let p = createVector(50+i*40, 40);
    let v = createVecotr(0, 0);
    let mo = new Mover(p, v, i+1);
    movers.pushback(mo);
  }
  createP('畫布上左鍵滑鼠，啟動一個風力！');
}

function draw() {
  background(51);
  
  let gravity = createVector(0, 0.1);

  for(mo of movers){
      let gravitym = p5.Vector.mult(gravity, mo.m);
      mo.applyforce(gravitym);
  }

  if (mouseIsPressed) {
    let wind = createVector(0.1, 0);
    for( mo of movers)
      mo.applyForce(wind);
  }

  for(mo of movers){
     mo.update();
     mo.display();
     mo.checkEdges();
  }
}
```

地球提供的是重力場，地球上的物體受到的重力是重力場乘上物體的質量，為此我們設計的程式應該要能反映不同的重力。

### 摩擦力

物體的摩擦力公式： -1 * mu * N * v

mu 是摩擦係數，N 是垂直方向的重力，v 是單位速度，也就是1(向量) 。

大部分的模擬簡化 N 為 1, mu 用 c 表示。摩擦力的計算如下：

```
let frictionMag = 0.01;
let friction = createVector(m.ve.x, m.ve.y);
friction.mult(-1);
friction.normalize();
friction.mult(frictionMag);
```

我們將這個計算放到剛才的程式片段中：

```js
   for( m of movers){
       let frictionMag = 0.01;
        let friction = createVector(m.ve.x, m.ve.y);
        friction.mult(-1);
        friction.normalize();
        friction.mult(frictionMag);
        m.applyForce(friction);
   }
```

### 空氣/流體摩擦力

空氣摩擦力的公式： - 1/2 * ro * v^2 * A * C * v

ro 是流體密度, v^2 是速率的平方，A 是垂直移動方向的面積，C是拉引係數。最後一個 v 是方向。

1/2 * ro * A * C 合在一起成為一個係數，我們只要計算摩擦力與速率的平方成正比，方向相反。

```js
let c = 0.1;
let s = m.ve.mag();
let dragMag = c * s * s;
let drag = createVector(m.ve.x, m.ve.y);
drag.mult(-1).normalize().mult(dragMag);
m.applyForce( drag );
```

在物體移動的軌跡中，不需要所有的流體都是一致的，因此我們需要一個能夠表示流體的物件：

```
class Liquid {
  constructor(x, y, w, h, c) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
  }

  // Is the Mover in the Liquid?
  contains(mover) {
    let l = mover.po;
    return l.x > this.x && l.x < this.x + this.w &&
      l.y > this.y && l.y < this.y + this.h;
  }

  // Calculate drag force
  calculateDrag(mover) {
    // Magnitude is coefficient * speed squared
    let speed = mover.ve.mag();
    let dragMagnitude = this.c * speed * speed;

    // Direction is inverse of velocity
    let dragForce = mover.ve.copy();
    dragForce.mult(-1);

    // Scale according to magnitude
    // dragForce.setMag(dragMagnitude);
    dragForce.normalize();
    dragForce.mult(dragMagnitude);
    return dragForce;
  }

  display() {
    noStroke();
    fill(50);
    rect(this.x, this.y, this.w, this.h);
  }
}
```

在2D的模擬環境，流體區域由 x, y, w, h 表示左上角座標及寬高。c 表示係數。

在sketch.js 中，我們要將流體拖力加上去：

```js
let movers = [];

// Liquid
let liquid;

function setup() {
  createCanvas(640, 360);
  reset();
  liquid = new Liquid(0, height / 2, width, height / 2, 0.1);

  createP("click mouse to reset");
}

function draw() {
  background(127);

  // Draw water
  liquid.display();

  for (let i = 0; i < movers.length; i++) {

    // Is the Mover in the liquid?
    if (liquid.contains(movers[i])) {
      // Calculate drag force
      let dragForce = liquid.calculateDrag(movers[i]);
      // Apply drag force to Mover
      movers[i].applyForce(dragForce);
    }

    // Gravity is scaled by mass here!
    let gravity = createVector(0, 0.1 * movers[i].m);
    // Apply gravity
    movers[i].applyForce(gravity);

    // Update and display
    movers[i].update();
    movers[i].display();
    movers[i].checkEdges();
  }

}
,x-
// Not working???
function mousePressed() {
  reset();
}

// Restart all the Mover objects randomly
function reset() {
  for (let i = 0; i < 9; i++) {
      let p = createVector(40+i*70, 0);
      let v = createVector(0, 0);
    movers[i] = new Mover(p, v, random(0.5, 3));
  }
}
```

### 萬有引力

重力是宇宙中最普遍存在的力。根據牛頓的計算，一個物體受到的重力可由二個物體間的萬有引力形成。二個星體，在不考慮其他星體的情況下，互相之間的引力公式為：

F =   G * m1 * m2 / (r * r)  

我們先模擬宇宙間有一個巨大質量的物體，姑且稱之為 attractor 。我們的 mover 與這個 attractor 之間的引力只作用在 mover 上，不發生在 attractor 。

Attractor 的建構子包含 position, mass, G。我們需要一個計算引力的函式 attract(mover)。

除此之外，為了提供滑鼠可以拖拉這個 attractor 的位置，我們要處理 mouseMoved, mousePressed, mouseDragged 以及 mouseRelease 。對應這四個動作，我們在 Attractor 中撰寫 hHover, hPress, hDrag 以及 stopDragging 四個函式。

attractor.js

```js
class Attractor {
  constructor() {
    this.po = createVector(width / 2, height / 2);
    this.m = 20;
    this.G = 1;
    this.dragOffset = createVector(0, 0);
    this.dragging = false;
    this.rollover = false;
  }

  attract(mover) {
    let force = p5.Vector.sub(this.po, mover.po);
    let distance = force.mag();
    distance = constrain(distance, 5, 25);

    let strength = (this.G * this.m * mover.m) / (distance * distance);
    force.setMag(strength);
    return force;
  }

  // Method to display
  display() {
    ellipseMode(CENTER);
    strokeWeight(4);
    stroke(0);
    if (this.dragging) {
      fill(50);
    } else if (this.rollover) {
      fill(100);
    } else {
      fill(175, 200);
    }
    ellipse(this.po.x, this.po.y, this.m * 2, this.m * 2);
  }

  // The methods below are for mouse interaction
  handlePress(mx, my) {
    let d = dist(mx, my, this.po.x, this.po.y);
    if (d < this.mass) {
      this.dragging = true;
      this.dragOffset.x = this.po.x - mx;
      this.dragOffset.y = this.po.y - my;
    }
  }

  handleHover(mx, my) {
    let d = dist(mx, my, this.po.x, this.po.y);
    if (d < this.m) {
      this.rollover = true;
    } else {
      this.rollover = false;
    }
  }

  stopDragging() {
    this.dragging = false;
  }

  handleDrag(mx, my) {
    if (this.dragging) {
      this.po.x = mx + this.dragOffset.x;
      this.po.y = my + this.dragOffset.y;
    }
  }
}
```

sketch.js

```js
let mover;

let attractor;

function setup() {
  createCanvas(640, 360);
  mover = new Mover(createVector(300, 100), createVector(0,0), 2);
  attractor = new Attractor();
}

function draw() {
  background(51);

  let force = attractor.attract(mover);
  mover.applyForce(force);
  mover.update();

  attractor.display();
  mover.display();
}

function mouseMoved() {
  attractor.handleHover(mouseX, mouseY);
}

function mousePressed() {
  attractor.handlePress(mouseX, mouseY);
}

function mouseDragged() {
  attractor.handleHover(mouseX, mouseY);
  attractor.handleDrag(mouseX, mouseY);
}

function mouseReleased() {
  attractor.stopDragging();
}
```

請各位練習，產生 7 個不同大小的 movers, 各自繞著 attractor 運動！

