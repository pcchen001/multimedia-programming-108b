---
title: Data & Data Visualization
date: 2020-04-21 15:00:00
tags:
- data
- data visualization
- ajax
- REST
categories:
- Application
---

本單元介紹資料處理以及資料視覺化的一些課題。

資料視覺化包含三個主題：資料取得、資料處理、資料的視覺化呈現。

網站的資料取得有三種情況：(1) 與網站同一位置的檔案 (2) 由自家伺服器提供的資料 (3) 第三方網站提供。

資料的處理首先要知道資料的型態，主要有(1) 字串 (2) csv/tsv 等表格資料 (3) JSON (4) bytes 。接著是去將資料做資訊抽取的處理，以便用最恰當的視覺化表示出來，目的是讓使用者一目了然，且能掌握資訊內涵。

資料視覺化經典的處理方式是曲線圖、圓餅圖、長條圖等，但有時若我們能多了解資訊內涵及多媒體技術，創新多元的視覺化會讓資料的呈現更豐富精彩！

p5.js 提供的資料下載函式分為二類：資料下載以及Ajax 。

我們也介紹資料下載的一些課題，包含 ajax, json, jsonp 等。

從網站取得資料，

P5.js 的輸入指令有下列幾個，我們要記得的是這些指令幾乎都是使用 fetch 的 Ajax 指令進行的，只不過包裝得更加簡單方便。

我們先從最簡單方便的指令開始看起。

### Input

Load data from URL/file : 

- [loadStrings()](https://p5js.org/reference/#/p5/loadStrings)
- [loadJSON()](https://p5js.org/reference/#/p5/loadJSON)
- [loadTable()](https://p5js.org/reference/#/p5/loadTable)
- [loadBytes()](https://p5js.org/reference/#/p5/loadBytes)

JavaScript Ajax like functions:

- [httpGet()](https://p5js.org/reference/#/p5/httpGet)
- [httpPost()](https://p5js.org/reference/#/p5/httpPost)
- [httpDo()](https://p5js.org/reference/#/p5/httpDo)

#### loadStrings() 從一個文字檔或是來源取得一個陣列的文字。請注意這個函式最後放在 preload() 裡面。

```js
let result;
function preload() {
  result = loadStrings('poem.txt');
}

function setup() {
  background(200);
  text(random(result), 10, 10, 80, 80);
}
```

注：random(陣列) 返回隨機的一個元素。

```
Invitation to Love
Paul Laurence Dunbar, 1872 - 1906

 Come when the nights are bright with stars
   Or when the moon is mellow;
Come when the sun his golden bars
   Drops on the hay-field yellow.
Come in the twilight soft and gray,
Come in the night or come in the day,
Come, O love, whene’er you may,
   And you are welcome, welcome.

You are sweet, O Love, dear Love,
You are soft as the nesting dove.
Come to my heart and bring it rest
As the bird flies home to its welcome nest.

Come when my heart is full of grief
   Or when my heart is merry;
Come with the falling of the leaf
   Or with the redd’ning cherry.
Come when the year’s first blossom blows,
Come when the summer gleams and glows,
Come with the winter’s drifting snows,
   And you are welcome, welcome.
```

載入檔案要使用 preload，確保在檔案下載完畢後才開始執行相關程式碼。

如果不在 preload() 中，則下載之後才能執行的程式碼要放在 callback function內 : 

```js
function setup() {
  loadStrings('poem.txt', pickString);
}

function pickString(result) {
  background(200);
  text(random(result), 10, 10, 80, 80);
}
```

#### loadTable從 csv 或是 tsv 檔讀入表格資料。讀入的表格用 p5.Table 物件來處理。

```js
let table;
let col, row;
function preload(){
   table = loadTable("mame.csv", "header");  // "header" 若沒有，則資料會包含 header

}
function setup() {
  createCanvas(400, 400);
   col = table.getColumnCount(); // 3
   row = table.getRowCount();    // 3, 若沒有 "header" 則為 4
  print(col +".."+ row);
    for(let r = 0; r < row; r++){
        arow = table.getRow(r);
        astr = "";
        for( let c = 0; c < col; c++){
            astr += arow.getString(c); astr +=" "
        }
        print(astr);
    }
  for(let c = 0; c < col; c++){
      acol = table.getColumn(c);
      for( ac of acol ) print( ac );
  }
}
```

mame.csv

```
id, name, age
1, good Animal, 12
2, bad animal, 44
7, monal animal, 99
```

p5.Table 的幾個重要函式說明如下：

1. getColumnCount(), getRowCount() 取得欄位數及資料筆數。 若 loadTable() 加上 'header'引數，則假設有一行欄位標題，因此資料不包含欄位標題。
2. getRow( i ) 取的第 i 筆資料，這會是一個 object. 因此要使用 arow.getString( j ) 來取得第 j 攔資料。
3. getColumn( j ) 取得第 j 個直欄的所有資料，這是一個 Array 。可使用 Array 的函式來處理。
4. 更多的函式說明請參閱[官方說明](https://p5js.org/reference/#/p5.Table)
5. 針對 p5.Table, table.getString(i, j) 可以取得 table 第 i 筆 第 j 欄的資料。針對 p5.TableRow, row.getString( j ) 。
6. 根據附註的資料，下載約翰霍普斯金大學醫學中心 Covid-19 每日確診時間軸資料，找出最近一天的最高10個國家的資料。

#### loadJSON 載入一個 json 的字串資料

```js
let earthquakes;
function preload() {
  // Get the most recent earthquake in the database
  let url =
   'https://earthquake.usgs.gov/earthquakes/feed/v1.0/' +
    'summary/all_day.geojson';
  earthquakes = loadJSON(url);
}

function setup() {
  noLoop();
}

function draw() {
  background(200);
  // Get the magnitude and name of the earthquake out of the loaded JSON
  let earthquakeMag = earthquakes.features[0].properties.mag;
  let earthquakeName = earthquakes.features[0].properties.place;
  ellipse(width / 2, height / 2, earthquakeMag * 10, earthquakeMag * 10);
  textAlign(CENTER);
  text(earthquakeName, 0, height - 30, width, 30);
}
```

#### loadBytes 載入一個檔案，將之以bytes 的陣列表示。

通常我們對於原始資料檔案大都會使用 bytes 來處理，例如聲音的取樣資料，一個取樣通常是2個 bytes, 一段錄音就會是一個 bytes 的陣列。

### 視覺化設計

最常見的視覺化是曲線圖，利用 loadTable 取的一個 table, 然後繪圖：

我們的資料為 1880 ~ 2018 年全球以及多個區域的平均溫度變化：

```
Year,Glob,NHem,SHem,24N-90N,24S-24N,90S-24S,64N-90N,44N-64N,24N-44N,EQU-24N,24S-EQU,44S-24S,64S-44S,90S-64S
1880,-.18,-.31,-.06,-.38,-.17,-.01,-.97,-.47,-.25,-.21,-.13,-.04,.05,.67
1881,-.10,-.19,-.01,-.32,.05,-.07,-.91,-.46,-.14,.03,.07,-.06,-.07,.60
1882,-.11,-.21,-.01,-.28,-.07,.02,-1.49,-.27,-.08,-.09,-.05,.02,.04,.63
1883,-.19,-.31,-.06,-.36,-.21,.01,-.35,-.59,-.22,-.24,-.17,-.01,.07,.50
1884,-.29,-.43,-.15,-.56,-.21,-.13,-1.32,-.59,-.40,-.23,-.19,-.19,-.02,.65
1885,-.31,-.40,-.24,-.58,-.15,-.25,-1.18,-.66,-.42,-.09,-.22,-.31,-.15,.82
1886,-.32,-.41,-.24,-.51,-.25,-.22,-1.25,-.49,-.39,-.24,-.26,-.22,-.20,.59
1887,-.35,-.41,-.30,-.46,-.32,-.28,-1.57,-.48,-.25,-.31,-.33,-.28,-.26,.32
1888,-.18,-.23,-.14,-.42,.07,-.29,-1.48,-.44,-.20,.07,.08,-.32,-.23,.16
1889,-.11,-.17,-.06,-.28,.05,-.16,-.86,-.21,-.21,.00,.10,-.16,-.15,.45
1890,-.37,-.42,-.33,-.47,-.42,-.22,-1.29,-.55,-.25,-.34,-.50,-.18,-.27,.15
1891,-.23,-.26,-.21,-.41,-.12,-.21,-1.28,-.29,-.31,-.03,-.21,-.18,-.24,-.15
1892,-.27,-.37,-.16,-.40,-.30,-.08,-1.32,-.59,-.10,-.33,-.28,.02,-.22,.09
1893,-.32,-.45,-.19,-.47,-.39,-.07,-.91,-.51,-.35,-.42,-.36,.03,-.21,-.04
1894,-.31,-.38,-.24,-.32,-.35,-.25,-1.37,-.26,-.11,-.48,-.23,-.16,-.35,-.37
1895,-.23,-.29,-.16,-.38,-.09,-.26,-.96,-.45,-.19,-.14,-.03,-.21,-.32,-.58
1896,-.11,-.20,-.03,-.36,.09,-.15,-1.25,-.35,-.15,.05,.13,-.15,-.13,.26
1897,-.12,-.17,-.07,-.34,.14,-.24,-.79,-.39,-.20,.10,.17,-.21,-.28,.41
1898,-.28,-.29,-.28,-.32,-.27,-.27,-1.25,-.16,-.20,-.24,-.29,-.29,-.21,.38
1899,-.19,-.21,-.16,-.21,-.16,-.20,-1.12,.00,-.13,-.20,-.12,-.18,-.22,.59
1900,-.09,-.09,-.09,-.19,.13,-.31,-.65,-.08,-.13,.07,.19,-.30,-.29,.27
1901,-.16,-.10,-.21,-.12,-.07,-.31,-.59,-.03,-.07,-.06,-.07,-.29,-.34,.26
1902,-.30,-.35,-.25,-.53,-.09,-.36,-1.64,-.42,-.32,-.07,-.12,-.30,-.43,-.04
1903,-.39,-.39,-.39,-.48,-.33,-.39,-.53,-.38,-.52,-.26,-.39,-.37,-.40,-.47
1904,-.49,-.50,-.49,-.51,-.52,-.44,-.31,-.56,-.53,-.48,-.56,-.39,-.49,-1.29
1905,-.29,-.31,-.26,-.39,-.15,-.36,-.20,-.18,-.59,-.17,-.13,-.34,-.37,-.31
1906,-.23,-.23,-.23,-.23,-.22,-.23,-.41,.00,-.34,-.23,-.21,-.21,-.23,-.69
1907,-.40,-.47,-.32,-.57,-.34,-.30,-.76,-.69,-.46,-.31,-.36,-.22,-.37,-1.17
1908,-.43,-.46,-.42,-.47,-.46,-.35,-.45,-.50,-.46,-.43,-.50,-.29,-.48,.70
1909,-.47,-.47,-.48,-.49,-.49,-.44,-.82,-.51,-.40,-.44,-.54,-.38,-.52,-.54
1910,-.43,-.44,-.43,-.39,-.51,-.37,-.72,-.16,-.46,-.51,-.51,-.32,-.45,.19
1911,-.43,-.40,-.47,-.38,-.44,-.47,-.36,-.36,-.41,-.41,-.46,-.43,-.52,.08
1912,-.36,-.45,-.25,-.57,-.19,-.35,-.64,-.71,-.47,-.27,-.12,-.35,-.29,-1.61
1913,-.35,-.43,-.27,-.41,-.32,-.34,-.66,-.25,-.46,-.46,-.18,-.27,-.40,-1.01
1914,-.16,-.19,-.12,-.23,-.08,-.18,-.59,-.07,-.25,-.13,-.04,-.16,-.18,-.34
1915,-.12,-.11,-.13,-.18,-.01,-.21,-.59,-.06,-.15,.01,-.03,-.14,-.23,-1.96
1916,-.33,-.36,-.31,-.29,-.44,-.21,-.32,-.43,-.20,-.46,-.42,-.21,-.17,-1.02
1917,-.44,-.54,-.35,-.41,-.65,-.18,-.65,-.37,-.39,-.72,-.58,-.22,-.09,.09
1918,-.28,-.37,-.20,-.38,-.33,-.13,-1.19,-.16,-.29,-.35,-.30,-.06,-.22,-.25
1919,-.27,-.33,-.20,-.40,-.20,-.22,-.85,-.44,-.27,-.22,-.18,-.15,-.32,.22
1920,-.25,-.25,-.25,-.19,-.26,-.31,.02,-.07,-.31,-.36,-.16,-.18,-.48,-1.12
1921,-.17,-.07,-.28,.06,-.25,-.31,-.03,.27,-.05,-.26,-.24,-.23,-.38,-.81
1922,-.27,-.24,-.30,-.20,-.32,-.27,-.39,-.30,-.09,-.30,-.33,-.25,-.28,-.25
1923,-.24,-.18,-.30,-.10,-.28,-.33,.13,-.03,-.22,-.29,-.27,-.26,-.41,-.52
1924,-.25,-.14,-.35,-.09,-.24,-.42,.28,-.15,-.17,-.22,-.26,-.39,-.46,-.51
1925,-.20,-.10,-.30,.00,-.23,-.37,-.16,.27,-.13,-.25,-.21,-.24,-.54,-.84
1926,-.08,.05,-.20,.06,.04,-.40,.49,.26,-.19,.03,.06,-.35,-.45,-.61
1927,-.20,-.10,-.29,-.10,-.13,-.38,-.04,-.12,-.11,-.09,-.17,-.31,-.45,-1.36
1928,-.19,-.07,-.30,-.04,-.15,-.39,.60,-.07,-.20,-.11,-.18,-.29,-.48,-2.10
1929,-.34,-.30,-.38,-.34,-.27,-.45,.02,-.51,-.34,-.24,-.30,-.42,-.44,-1.05
1930,-.14,.02,-.29,.09,-.09,-.43,.44,.16,-.07,-.07,-.12,-.34,-.47,-2.57
1931,-.10,.05,-.23,.03,.05,-.43,.41,.09,-.12,.08,.01,-.42,-.40,-.39
1932,-.16,-.05,-.27,.05,-.19,-.34,.23,.31,-.17,-.19,-.18,-.21,-.53,-.96
1933,-.30,-.26,-.33,-.24,-.31,-.34,-.43,-.38,-.10,-.29,-.32,-.22,-.49,-1.12
1934,-.14,-.01,-.27,.18,-.28,-.26,.65,.41,-.10,-.28,-.28,-.16,-.38,-.54
1935,-.20,-.09,-.32,-.02,-.21,-.38,.19,.06,-.13,-.18,-.24,-.31,-.42,-1.69
1936,-.16,-.04,-.28,-.03,-.15,-.30,.14,.19,-.22,-.05,-.25,-.27,-.33,-.01
1937,-.04,.12,-.20,.18,-.07,-.22,.97,.10,.01,.03,-.16,-.20,-.25,.39
1938,-.03,.12,-.21,.34,-.26,-.10,1.09,.45,.05,-.19,-.34,-.06,-.11,-.84
1939,-.03,.08,-.15,.23,-.16,-.13,.45,.35,.10,-.13,-.18,-.05,-.19,-1.58
1940,.11,.17,.07,.11,.25,-.07,.86,.06,-.07,.25,.25,.03,-.20,-.03
1941,.18,.24,.15,.02,.47,-.05,-.23,-.04,.12,.55,.40,.03,-.15,-.44
1942,.05,.11,.00,.09,.07,-.01,.25,-.01,.10,.13,.02,.06,-.05,-.87
1943,.07,.15,-.02,.32,-.10,.04,1.08,.29,.13,-.10,-.09,.07,.00,1.16
1944,.21,.27,.15,.37,.19,.07,.94,.51,.12,.13,.26,.19,-.09,-.21
1945,.09,.09,.09,.08,.18,-.02,.36,-.02,.06,.12,.23,.04,.00,-1.85
1946,-.07,.04,-.17,.06,-.05,-.24,-.22,-.01,.19,-.01,-.09,-.22,-.24,.02
1947,-.04,.06,-.14,.12,-.07,-.15,.85,-.12,.05,-.02,-.11,-.19,-.08,.03
1948,-.10,-.01,-.20,.13,-.20,-.21,.07,.34,.02,-.22,-.17,-.19,-.19,-1.01
1949,-.10,-.03,-.18,.11,-.20,-.19,.13,.17,.06,-.23,-.17,-.17,-.14,-1.28
1950,-.18,-.17,-.20,-.09,-.29,-.12,-.01,-.32,.03,-.30,-.29,-.06,-.16,-.72
1951,-.06,.05,-.17,.08,-.06,-.20,-.02,.03,.14,.00,-.13,-.22,-.16,-.18
1952,.01,.06,-.03,.09,.01,-.08,.11,-.07,.19,.00,.03,-.09,-.02,-.38
1953,.07,.23,-.08,.33,.07,-.19,.81,.36,.17,.08,.05,-.08,-.24,-1.34
1954,-.15,-.04,-.25,.02,-.22,-.21,.51,-.21,.01,-.14,-.30,-.17,-.20,-.77
1955,-.14,-.10,-.20,.02,-.33,-.05,-.45,-.08,.23,-.29,-.38,-.16,-.08,1.27
1956,-.20,-.26,-.16,-.25,-.33,.00,-.29,-.47,-.09,-.28,-.37,-.13,.02,.67
1957,.04,.04,.04,.03,.07,.01,.03,.23,-.09,.06,.09,-.09,-.01,.45
1958,.07,.17,-.03,.11,.20,-.15,-.11,.18,.13,.27,.14,-.09,-.08,-.55
1959,.03,.11,-.06,.15,.07,-.16,.42,.17,.04,.07,.08,-.03,-.26,-.37
1960,-.02,.08,-.12,.09,.03,-.20,.32,-.06,.11,.07,-.01,-.10,-.07,-.79
1961,.05,.08,.02,.18,-.03,.04,-.16,.37,.17,-.06,.01,.14,-.18,.14
1962,.04,.15,-.08,.29,-.05,-.10,.64,.33,.15,-.04,-.05,.09,-.07,-.81
1963,.07,.16,-.02,.17,.12,-.10,-.01,.33,.13,.14,.10,-.11,-.17,.08
1964,-.20,-.19,-.20,-.24,-.13,-.24,-.66,-.22,-.11,-.13,-.14,-.30,-.07,-.38
1965,-.10,-.13,-.08,-.16,-.03,-.14,-.18,-.23,-.11,-.07,.01,-.23,-.03,-.08
1966,-.05,.00,-.09,-.12,.08,-.15,-.69,-.16,.07,.17,.00,-.23,-.14,.13
1967,-.02,.03,-.08,.13,-.14,-.01,.46,.26,-.07,-.11,-.17,-.09,-.02,.28
1968,-.08,-.06,-.09,-.09,-.05,-.09,-.22,-.02,-.09,-.01,-.09,-.14,.00,-.11
1969,.07,-.01,.15,-.26,.33,.04,.05,-.59,-.16,.35,.31,.00,.13,.01
1970,.03,-.03,.09,-.10,.07,.12,-.17,-.13,-.06,.07,.06,.10,.03,.36
1971,-.09,-.16,-.03,-.06,-.26,.11,-.07,.00,-.11,-.30,-.22,.02,.16,.33
1972,.01,-.18,.22,-.35,.17,.17,-.40,-.50,-.23,.05,.28,.16,-.02,.60
1973,.16,.10,.22,.12,.18,.17,.21,.26,.01,.06,.29,.22,.07,.28
1974,-.08,-.20,.03,-.17,-.20,.17,-.26,-.10,-.19,-.24,-.17,.22,-.09,.55
1975,-.02,-.06,.02,.12,-.24,.15,.20,.36,-.06,-.31,-.16,.14,.14,.22
1976,-.11,-.21,.00,-.26,-.12,.07,-.05,-.33,-.29,-.14,-.10,.11,.22,-.37
1977,.17,.11,.23,.13,.15,.23,.21,.22,.05,.08,.23,.21,.31,.15
1978,.07,.02,.11,-.03,.08,.14,-.03,.00,-.04,.10,.07,.21,.13,-.11
1979,.16,.08,.24,-.03,.26,.22,-.56,.06,.08,.24,.27,.29,.33,-.24
1980,.27,.16,.38,.07,.29,.45,.35,.01,.01,.31,.28,.35,.37,.96
1981,.33,.39,.27,.51,.18,.35,1.34,.78,.08,.21,.16,.28,.38,.52
1982,.14,.05,.22,-.07,.27,.16,-.30,.07,-.08,.24,.30,.18,.32,-.23
1983,.31,.26,.38,.24,.43,.23,.34,.67,-.06,.29,.58,.21,.41,-.03
1984,.16,.04,.29,.03,.18,.28,.39,.08,-.11,.06,.30,.16,.32,.59
1985,.12,-.01,.25,-.03,.09,.31,.37,-.27,.00,.03,.16,.30,.45,.09
1986,.18,.13,.24,.11,.22,.21,.10,.25,.04,.16,.29,.25,.25,-.05
1987,.34,.27,.41,.08,.58,.26,-.22,.14,.13,.55,.62,.32,.25,.10
1988,.41,.38,.44,.42,.38,.44,.82,.51,.23,.32,.45,.38,.20,1.14
1989,.29,.29,.29,.43,.15,.34,.45,.67,.27,.09,.22,.36,.30,.36
1990,.44,.51,.37,.62,.36,.37,.67,.83,.47,.35,.38,.37,.37,.33
1991,.41,.41,.41,.47,.37,.41,.83,.61,.27,.32,.42,.32,.29,1.01
1992,.22,.12,.32,.08,.29,.28,-.11,.36,-.04,.20,.38,.21,.33,.41
1993,.24,.19,.29,.14,.31,.25,.69,.24,-.10,.27,.35,.27,.39,-.14
1994,.31,.36,.26,.44,.30,.20,.43,.46,.43,.25,.34,.27,.23,-.10
1995,.45,.58,.31,.71,.43,.21,1.41,.96,.33,.40,.46,.29,.15,.06
1996,.34,.28,.39,.27,.32,.43,.88,.20,.12,.30,.33,.34,.29,1.07
1997,.47,.53,.41,.55,.51,.33,.82,.89,.26,.50,.52,.42,.37,-.06
1998,.62,.73,.52,.82,.70,.34,1.04,.93,.67,.60,.79,.39,.31,.20
1999,.40,.50,.30,.73,.21,.32,.51,.83,.75,.15,.27,.47,.19,.05
2000,.40,.50,.30,.71,.24,.30,1.16,.76,.55,.19,.29,.42,.10,.32
2001,.53,.63,.43,.80,.41,.43,1.10,.81,.70,.39,.43,.57,.24,.39
2002,.62,.71,.54,.83,.58,.47,1.39,.97,.57,.54,.63,.47,.32,.81
2003,.61,.73,.49,.82,.61,.38,1.56,.95,.51,.58,.64,.44,.24,.47
2004,.53,.67,.40,.75,.54,.31,.68,.93,.67,.55,.53,.48,.23,-.11
2005,.67,.83,.51,.99,.61,.43,2.00,1.17,.55,.59,.62,.49,.19,.75
2006,.61,.78,.45,.94,.54,.38,1.62,1.05,.66,.54,.55,.51,.20,.32
2007,.64,.81,.46,1.07,.45,.45,1.90,1.30,.67,.42,.47,.49,.07,1.11
2008,.51,.64,.39,.86,.36,.37,1.37,1.02,.60,.31,.41,.53,.09,.42
2009,.63,.69,.57,.73,.67,.48,1.23,.58,.66,.64,.70,.60,.15,.79
2010,.70,.86,.53,.97,.67,.46,1.98,.85,.73,.70,.63,.63,.20,.41
2011,.58,.70,.46,.92,.35,.55,2.09,.89,.56,.37,.34,.65,.18,.97
2012,.61,.76,.47,.96,.50,.41,1.88,.88,.72,.45,.55,.56,.19,.34
2013,.64,.74,.54,.86,.56,.53,1.17,1.02,.67,.56,.55,.63,.23,.79
2014,.73,.89,.56,1.04,.65,.52,1.78,1.12,.75,.67,.62,.71,.18,.58
2015,.86,1.12,.61,1.24,.91,.40,1.66,1.44,.99,.93,.89,.73,.18,-.27
2016,.98,1.26,.71,1.50,.97,.49,3.05,1.41,1.06,.92,1.02,.66,.25,.42
2017,.90,1.11,.68,1.32,.79,.61,2.21,1.35,1.02,.81,.77,.75,.35,.68
2018,.82,.99,.66,1.19,.64,.70,1.87,1.09,1.03,.69,.59,.78,.37,1.07
```

```js
let table;
function preload() {
  table = loadTable('ZonAnn.Ts+dSST.csv', 'header');
}

function setup() {
  createCanvas(600, 400);
  background(0);
  stroke(255);
  noFill();
  beginShape();
  for (var i = 0; i < table.getRowCount(); i++) {
    let row = table.getRow(i);
    let temp = 14 + row.getNum('Glob');
    let x = map(i, 0, table.getRowCount() - 1, 0, width);
    let y = map(temp, 13.5, 15, height, 0);
    vertex(x, y);
  }
  endShape();
}
```

這是 全球的，若要除了 Glob 外，增加 NHem,SHem,24N-90N, 24S-24N 四個區域，請試試看。



 js 有不少資料視覺化的 api 可用，最常用的有 [chart.js](http://chartjs.org/) 以及 [d3.js](https://d3js.org/)

我們初看一個使用 chart.js 的範例：

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Coding Train: Data and APIs Project 1</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  </head>
  <body>
    <h1>Global Average Temperatures 1880-2018</h1>
    <canvas id="myChart" width="400" height="200"></canvas>

    <script>
      // Data from: https://data.giss.nasa.gov/gistemp/
      // Mean from: https://earthobservatory.nasa.gov/world-of-change/DecadalTemp

      window.addEventListener('load', setup);

      async function setup() {
        const ctx = document.getElementById('myChart').getContext('2d');
        const dataTemps = await getData();
        const myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: dataTemps.years,
            datasets: [
              {
                label: '全球氣溫 in °C',
                data: dataTemps.temps,
                fill: false,
                borderColor: 'rgba(255, 99, 132, 1)',
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
                borderWidth: 1
              },
              {
                label: '北半球氣溫 in °C',
                data: dataTemps.northern,
                fill: false,
                borderColor: 'rgba(99, 132, 255, 1)',
                backgroundColor: 'rgba(99, 132, 255, 0.5)',
                borderWidth: 1
              },
              {
                label: '南半球氣溫 in °C',
                data: dataTemps.southern,
                fill: false,
                borderColor: 'rgba(99, 255, 132, 1)',
                backgroundColor: 'rgba(99, 255, 132, 0.5)',
                borderWidth: 1
              }
            ]
          },
          options: {}
        });
      }

      async function getData() {
        // const response = await fetch('testdata.csv');
        const response = await fetch('ZonAnn.Ts+dSST.csv');
        const data = await response.text();
        const years = [];
        const temps = [];
        const northern = [];
        const southern = [];
        const rows = data.split('\n').slice(1);
        rows.forEach(row => {
          const cols = row.split(',');
          years.push(cols[0]);
          temps.push(14 + parseFloat(cols[1]));
          northern.push(14 + parseFloat(cols[2]));
          southern.push(14 + parseFloat(cols[3]));
        });
        return { years, temps, northern, southern };
      }
    </script>
  </body>
</html>
```

請各位練習使用 新冠病毒的資料。



### 地圖應用

支援網頁地圖的服務早期以 google map 為主流，近年來 open source 服務愈來愈完整，同學多了許多選項。我們先介紹專門為支援 p5.js 開發的 [mappa](https://mappa.js.org/) 服務的應用。

使用 api, 當然要先匯入，假設我們使用 CDN，要在 index.html 加上：

```html
	<script src="https://cdn.jsdelivr.net/npm/mappa-mundi@0.0.4"></script>
```

接著，sketch.js 範例如下：

```js
const mappa = new Mappa('Leaflet');
let mymap;
const api_url = 'https://api.wheretheiss.at/v1/satellites/25544';

let canvas;
let issImg;
const options = {
  lat: 0,
  lng: 0,
  zoom: 1.5,
  style: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png'
};
let x = 0;
let y = 0;

function preload() {
  issImg = loadImage('iss200.png');  // 太空站的小圖示
}

function setup() {
  canvas = createCanvas(800, 600);
  mymap = mappa.tileMap(options);  // 宣告 mappa 地圖
  mymap.overlay(canvas);           // 疊在 canvas 上
  getData();
  setInterval(getData, 1000);
}

function getData() {
  loadJSON(api_url, gotData);     // callback function: gotData
}

function gotData(data) {
  console.log(data);
  const pix = mymap.latLngToPixel(data.latitude, data.longitude);  // GPS 對應 畫面 x, y
  x = pix.x;
  y = pix.y;
}

function draw() {
  clear();
  image(issImg, x, y, 50, 32);
}
```

我們做一點點改進，在地圖上繪出軌跡並且展示出飛行的方向速度。

```js
const mappa = new Mappa('Leaflet');
let mymap;
const api_url = 'https://api.wheretheiss.at/v1/satellites/25544';

let canvas;
let issImg;
let firstTime = true;

let pos;
let history = [];  // 紀錄軌跡

function preload() {
  issImg = loadImage('iss200.png');
}

function setup() {
  canvas = createCanvas(800, 600);
  getData();
  setInterval(getData, 1000);
}

function getData() {
  loadJSON(api_url, gotData);
}

function gotData(data) {
  if (firstTime) {
    const options = {
      lat: data.latitude,
      lng: data.longitude,
      zoom: 7,
      style: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png'
    };
    mymap = mappa.tileMap(options);
    mymap.overlay(canvas);
    mymap.onChange(render);    // 設定事件 change 的處理函式
    firstTime = false;
  }
  history.push(data);
  render();
}

function render() {
  clear();

  // Draw a path
  strokeWeight(mymap.zoom());
  stroke(100);
  noFill();
  beginShape();
  for (let data of history) {
    const pix = mymap.latLngToPixel(data.latitude, data.longitude);
    vertex(pix.x, pix.y);
  }
  endShape();

  // Get the last spot is current
  const current = history[history.length - 1];
  const pix = mymap.latLngToPixel(current.latitude, current.longitude);

  // Get the second to last spot and extrapolate a velocity vector?
  if (history.length > 1) {
    const previous = history[history.length - 2];
    const prevPix = mymap.latLngToPixel(previous.latitude, previous.longitude);
    offset = createVector(pix.x - prevPix.x, pix.y - prevPix.y);
    offset.setMag(50);

    // Renders a vector object 'v' as an arrow and a position 'loc'
    push();
    let arrowsize = 20;
    translate(pix.x, pix.y);
    rotate(offset.heading());
    let len = offset.mag();
    line(0, 0, len, 0);
    line(len, 0, len - arrowsize, arrowsize / 2);
    line(len, 0, len - arrowsize, -arrowsize / 2);
    pop();
  }

  imageMode(CENTER);
  image(issImg, pix.x, pix.y, 50, 32);
}
```



作業： 

利用地圖及氣象資料，做出一個互動的台灣風力、風向、氣溫的視覺化資訊圖。

利用新冠疫情資料，做出一個互動的病例趨勢變化圖。



Data Resources:

1. 約翰霍普斯金大學醫學中心 Covid-19 每日確診時間軸資料

   [confirmed.csv](https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv)

2. ISS 國際太空站即時資料

   [iss.json](https://api.wheretheiss.at/v1/satellites/25544)

3. 地震資料

   [earchquake.json](https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson)

4. 空氣品質 docs.openaq.org 

   

5. 其他

