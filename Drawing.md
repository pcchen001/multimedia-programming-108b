---
title: 基礎繪圖 2D 
date: 2020-02-02 20:47:55
tags: 
- syntax
- drawing
categories:
- drawing
mathjax: true 
---
# A. Drawing 基礎繪圖



## 畫面座標

https://p5js.org/learn/coordinate-system-and-shapes.html

![img](https://p5js.org/assets/learn/coordinate-system-and-shapes/images/drawing-01.png)

<p align="center">數學上的一般座標系統</p>




![img](https://p5js.org/assets/learn/coordinate-system-and-shapes/images/drawing-03.svg)

二個座標的對照

## 2D 基本繪圖

- [point()](https://p5js.org/reference/#/p5/point)
- [line()](https://p5js.org/reference/#/p5/line)
- [rect()](https://p5js.org/reference/#/p5/rect)
- [ellipse()](https://p5js.org/reference/#/p5/ellipse)
- [circle()](https://p5js.org/reference/#/p5/circle)
- [quad()](https://p5js.org/reference/#/p5/quad)
- [square()](https://p5js.org/reference/#/p5/square)
- [triangle()](https://p5js.org/reference/#/p5/triangle)
- [arc()](https://p5js.org/reference/#/p5/arc)

### Attributes

- [strokeWeight()](https://p5js.org/reference/#/p5/strokeWeight)
- [smooth()](https://p5js.org/reference/#/p5/smooth)
- [noSmooth()](https://p5js.org/reference/#/p5/noSmooth)
- [ellipseMode()](https://p5js.org/reference/#/p5/ellipseMode)
- [rectMode()](https://p5js.org/reference/#/p5/rectMode)
- [strokeCap()](https://p5js.org/reference/#/p5/strokeCap)
- [strokeJoin()](https://p5js.org/reference/#/p5/strokeJoin)

## Color

### Setting

- [noStroke()](https://p5js.org/reference/#/p5/noStroke)
- [stroke()](https://p5js.org/reference/#/p5/stroke)
- [fill()](https://p5js.org/reference/#/p5/fill)
- [noFill()](https://p5js.org/reference/#/p5/noFill)
- [background()](https://p5js.org/reference/#/p5/background) 給背景色彩，一般用來動畫的影格清除。
- [clear()](https://p5js.org/reference/#/p5/clear) 清除掉整個畫布。
- [erase()](https://p5js.org/reference/#/p5/erase) 相當於 fill(255, opq) 。
- [noErase()](https://p5js.org/reference/#/p5/noErase) 取消 erase() 

### Creating & Reading

- [alpha()](https://p5js.org/reference/#/p5/alpha)
- [blue()](https://p5js.org/reference/#/p5/blue)
- [brightness()](https://p5js.org/reference/#/p5/brightness)
- [color()](https://p5js.org/reference/#/p5/color)
- [green()](https://p5js.org/reference/#/p5/green)
- [hue()](https://p5js.org/reference/#/p5/hue)
- [lerpColor()](https://p5js.org/reference/#/p5/lerpColor)
- [lightness()](https://p5js.org/reference/#/p5/lightness)
- [red()](https://p5js.org/reference/#/p5/red)
- [saturation()](https://p5js.org/reference/#/p5/saturation)
- [colorMode()](https://p5js.org/reference/#/p5/colorMode), colorMode(RGB, 255) 或是 colorMode(HSB, 100) 或是colorMode(HSB, 360, 100, 100)
- [p5.Color](https://p5js.org/reference/#/p5.Color)

P5JS 的色彩設定包含 r, g, b, a 四層，可用 (r,g,b,a) 或是(h, s, b, a) 來表示。colorMode() 用來設定指定色彩時的模型以及數值尺值。

## Curves

- [bezier()](https://p5js.org/reference/#/p5/bezier)
- [bezierDetail()](https://p5js.org/reference/#/p5/bezierDetail)
- [bezierPoint()](https://p5js.org/reference/#/p5/bezierPoint)
- [bezierTangent()](https://p5js.org/reference/#/p5/bezierTangent)
- [curve()](https://p5js.org/reference/#/p5/curve)
- [curveDetail()](https://p5js.org/reference/#/p5/curveDetail)
- [curveTightness()](https://p5js.org/reference/#/p5/curveTightness)
- [curvePoint()](https://p5js.org/reference/#/p5/curvePoint)
- [curveTangent()](https://p5js.org/reference/#/p5/curveTangent)

P5JS 曲線主要有 貝茲曲線及一般曲線。Point() 及 Tangent() 提供了我們在曲線上放置東西或是沿著曲線移動所需要的座標。

一條 bezier 曲線由四個點決定，二個端點二個控制點。控制點在內部。

一條curve 曲線也是由四個點決定，二個端點二個控制點，但是控制點在外部。

練習：

1. 樹葉，燭光。
2. 曲線山丘
3. 動畫沿著曲線移動

## Typography 字形學

| Attributes                                                   | Loading & Displaying                                   |
| ------------------------------------------------------------ | ------------------------------------------------------ |
| [textAlign()](https://p5js.org/reference/#/p5/textAlign)     | [loadFont()](https://p5js.org/reference/#/p5/loadFont) |
| [textLeading()](https://p5js.org/reference/#/p5/textLeading) | [text()](https://p5js.org/reference/#/p5/text)         |
| [textSize()](https://p5js.org/reference/#/p5/textSize)       | [textFont()](https://p5js.org/reference/#/p5/textFont) |
| [textStyle()](https://p5js.org/reference/#/p5/textStyle)  NORMALITALIC,BOLD,BOLDITALIC | Content                                                |
| [textWidth()](https://p5js.org/reference/#/p5/textWidth)     | [p5.Font](https://p5js.org/reference/#/p5.Font)        |
| [textAscent()](https://p5js.org/reference/#/p5/textAscent)   | Content                                                |
| [textDescent()](https://p5js.org/reference/#/p5/textDescent) | Content                                                |

在畫布上擺上文字，我們需要有字形檔。字形檔的格式常見的是otf檔。

載入字形檔的指令 myFont = loadFont('assets/inconsolata.otf'); 必須要放在 preload() 函式裡。若要放在其他地方，那麼便需要以 callback 的方式處理之後會用到此字形的指令，否則會有執行時拿不到字形的問題！

參見 [loadFont()](https://p5js.org/reference/#/p5/loadFont) 說明。

exit: Ctrl+↩

```
let myFont;
function preload() {
	myFont = loadFont('assets/inconsolata.otf');
}
function setup() {
	fill('#ED225D');  
	textFont(myFont);  
	textSize(36);  
	text('p5*js', 10, 50);
}
```

不放在 preload() 的範例：

```
function setup() {
  loadFont('assets/inconsolata.otf', drawText);
}

function drawText(font) {
  fill('#ED225D');
  textFont(font, 36);
  text('p5*js', 10, 50);
}
```

文字檔案的載入可以使用 loadStrings()取得一行一行的字串，關於字串的處理以及資料的處理及視覺化，我們在稍後的章節再介紹。

[loadStrings()](https://p5js.org/reference/#/p5/loadStrings)

由檔案(或是網路位置)載入字串：[請注意，需要在伺服器下執行，否則瀏覽器可能會下載錯誤！]

```
 let ss;
 function preload(){
  　ss = loadStrings("filename");
}
```

若不在 preload() 中讀取檔案，則需使用 callback

```
function setup(){
    losdStrings("filename", callback);
}
function callback(result){
    background(200);
    let ind = floor( random(result.length));
    text(result[ind], 10, 10);
}
```



## Image Loading & Displaying

- [loadImage()](https://p5js.org/reference/#/p5/loadImage)
- [image()](https://p5js.org/reference/#/p5/image)
- [tint()](https://p5js.org/reference/#/p5/tint)
- [noTint()](https://p5js.org/reference/#/p5/noTint)
- [imageMode()](https://p5js.org/reference/#/p5/imageMode)

## Transform

所有的繪圖都提供三個基本的線性處理：平移、旋轉、縮放。任何一個座標都可以透過一個轉換矩陣來達到這三個動作的目的，因此在觀念上，我們可以先將座標系統

- [applyMatrix()](https://p5js.org/reference/#/p5/applyMatrix)
- [resetMatrix()](https://p5js.org/reference/#/p5/resetMatrix)
- [rotate()](https://p5js.org/reference/#/p5/rotate) 旋轉，單位徑度
- [rotateX()](https://p5js.org/reference/#/p5/rotateX)
- [rotateY()](https://p5js.org/reference/#/p5/rotateY)
- [rotateZ()](https://p5js.org/reference/#/p5/rotateZ)
- [scale()](https://p5js.org/reference/#/p5/scale) 縮放
- [shearX()](https://p5js.org/reference/#/p5/shearX) 歪斜(x方向)
- [shearY()](https://p5js.org/reference/#/p5/shearY) 歪斜(y方向)
- [translate()](https://p5js.org/reference/#/p5/translate) 平移

### Description of push() & pop()

座標系統在進行一連串的轉換時，我們會使用 push() 及 pop() 來存放目前的座標系統設定，並且繼續新的座標系統轉換。在 P5JS ， push() 及 pop() 除了 matrix 之外，還另外儲存了其他的系統狀態，相關的狀態包含由下列指令所影響的狀態：‌

[push()](https://p5js.org/reference/#/p5/push) stores information related to the current transformation state and style settings controlled by the following functions: [fill()](https://p5js.org/reference/#/p5/fill), [noFill()](https://p5js.org/reference/#/p5/noFill), [noStroke()](https://p5js.org/reference/#/p5/noStroke), [stroke()](https://p5js.org/reference/#/p5/stroke), [tint()](https://p5js.org/reference/#/p5/tint), [noTint()](https://p5js.org/reference/#/p5/noTint), [strokeWeight()](https://p5js.org/reference/#/p5/strokeWeight), [strokeCap()](https://p5js.org/reference/#/p5/strokeCap), [strokeJoin()](https://p5js.org/reference/#/p5/strokeJoin), [imageMode()](https://p5js.org/reference/#/p5/imageMode), [rectMode()](https://p5js.org/reference/#/p5/rectMode), [ellipseMode()](https://p5js.org/reference/#/p5/ellipseMode), [colorMode()](https://p5js.org/reference/#/p5/colorMode), [textAlign()](https://p5js.org/reference/#/p5/textAlign), [textFont()](https://p5js.org/reference/#/p5/textFont), [textSize()](https://p5js.org/reference/#/p5/textSize), [textLeading()](https://p5js.org/reference/#/p5/textLeading), [applyMatrix()](https://p5js.org/reference/#/p5/applyMatrix), [resetMatrix()](https://p5js.org/reference/#/p5/resetMatrix), [rotate()](https://p5js.org/reference/#/p5/rotate), [scale()](https://p5js.org/reference/#/p5/scale), [shearX()](https://p5js.org/reference/#/p5/shearX), [shearY()](https://p5js.org/reference/#/p5/shearY), [translate()](https://p5js.org/reference/#/p5/translate), [noiseSeed()](https://p5js.org/reference/#/p5/noiseSeed).

In WEBGL mode additional style settings are stored. These are controlled by the following functions: [setCamera()](https://p5js.org/reference/#/p5/setCamera), [ambientLight()](https://p5js.org/reference/#/p5/ambientLight), [directionalLight()](https://p5js.org/reference/#/p5/directionalLight), [pointLight()](https://p5js.org/reference/#/p5/pointLight), [texture()](https://p5js.org/reference/#/p5/texture), [specularMaterial()](https://p5js.org/reference/#/p5/specularMaterial), [shininess()](https://p5js.org/reference/#/p5/shininess), [normalMaterial()](https://p5js.org/reference/#/p5/normalMaterial) and [shader()](https://p5js.org/reference/#/p5/shader).

我們會在動畫原理介紹之後，利用這個功能設計多媒體的各種動作。

## 旋轉角度的計算( Trigonometry )

- [atan()](https://p5js.org/reference/#/p5/atan)由斜率計算角度
- [atan2()](https://p5js.org/reference/#/p5/atan2) 由 y/x 計算角度( y, x )  可以有正負 
- [cos()](https://p5js.org/reference/#/p5/cos)
- [sin()](https://p5js.org/reference/#/p5/sin)
- [tan()](https://p5js.org/reference/#/p5/tan) 計算斜率
- [degrees()](https://p5js.org/reference/#/p5/degrees)
- [radians()](https://p5js.org/reference/#/p5/radians)
- [angleMode()](https://p5js.org/reference/#/p5/angleMode) DEGREES / RADIANS 預設 RADIANS
- [acos()](https://p5js.org/reference/#/p5/acos)
- [asin()](https://p5js.org/reference/#/p5/asin)