---
title: animation
date: 2020-02-23 20:47:15
tags:
---

# 3. 動畫

動畫的基本原理就是不斷的繪出一張一張的畫面，每秒 24 張以上，就能夠騙過眼睛。P5 使用 draw() 來實現動畫。前二個單元我們已經稍微接觸過動畫，本單元更進一步。

我們在第一單元介紹了 2D 的繪圖，第二單元介紹了 3D 的繪圖。我們也介紹了Transformation 的各種指令，如平移、旋轉、縮放等。這一個單元我們打算在這個基礎上，進行動態畫面的程式設計。

我們先綜合複習一下　2D 跟 3D 的　Transformation。

### 2D

我們來設計一隻小鳥的展翅飛行。用3個 box。

Transformation Animation 練習：

1. 時鐘
2. 機器臂
3. 齒輪
4. 衛星軌跡
5. 小草

### 3D

1. 小小鳥
2. 行星系

------



## Vector 向量

在空間中的移動或變形，通常需要參考座標系統，而在座標系統中，位置或位置的改變都是以座標向量為基礎的，因此，我們先花一點時間介紹向量，特別是 P5js 提供的向量類別 p5.Vector。

------

YouTube 的**The Coding Train** 頻道剛好有 Vector 系列的影片，大家可以參考！[Ｔｈｅ　Ｎａｔｕｒｅ　ｏｆ　Ｃｏｄｅ　２](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6ZV4yEcW3uDwOgGXKUUsPOM)

[你也可以到 NatureOfCode網頁參考！](https://natureofcode.com/book/chapter-1-vectors/)

------



## p5.Vector

https://p5js.org/reference/#/p5.Vector

p5.Vector 是一個類別，利用 createVector(x, y[, z]); 建立一個二或三維的向量。類別提供許多方便使用的計算函式，方便向量計算。特別是角度跟向量的轉換特別好用。

#### Methods

要留意的是許多方法都有靜態及非靜態二種，以 add()作例子， p5.Vector.add(v1, v2) 返回相加後的向量。 v1.add(v2) 是 v1 更改為加上 v2 之後。



| 函式                                                         | 說明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [toString()](https://p5js.org/reference/#/p5.Vector/toString) | Returns a string representation of a vector v by calling String(v) or v.toString(). This method is useful for logging vectors in the console. |
| [set()](https://p5js.org/reference/#/p5.Vector/set)          | Sets the x, y, and z component of the vector using two or three separate variables, the data from a [p5.Vector](https://p5js.org/reference/#/p5.Vector), or the values from a float array. |
| [copy()](https://p5js.org/reference/#/p5.Vector/copy)        | Gets a copy of the vector, returns a [p5.Vector](https://p5js.org/reference/#/p5.Vector) object. |
| [add()](https://p5js.org/reference/#/p5.Vector/add)          | 相加                                                         |
| [sub()](https://p5js.org/reference/#/p5.Vector/sub)          | 相減                                                         |
| [mult()](https://p5js.org/reference/#/p5.Vector/mult)        | 向量乘一個純量。　Multiply the vector by a scalar. The static version of this method creates a new [p5.Vector](https://p5js.org/reference/#/p5.Vector) while the non static version acts on the vector directly. See the examples for more context. |
| [div()](https://p5js.org/reference/#/p5.Vector/div)          | 向量除一個純量。　Divide the vector by a scalar. The static version of this method creates a new [p5.Vector](https://p5js.org/reference/#/p5.Vector) while the non static version acts on the vector directly. See the examples for more context. |
| [mag()](https://p5js.org/reference/#/p5.Vector/mag)          | 向量長。Calculates the magnitude (length) of the vector and returns the result as a float (this is simply the equation sqrt(x*x + y*y + z*z).) |
| [magSq()](https://p5js.org/reference/#/p5.Vector/magSq)      | 向量長平方。Calculates the squared magnitude of the vector and returns the result as a float (this is simply the equation *(x\*x + y\*y + z\*z)*.) Faster if the real length is not required in the case of comparing vectors, etc. |
| [dot()](https://p5js.org/reference/#/p5.Vector/dot)          | 內積。　Calculates the dot product of two vectors. The version of the method that computes the dot product of two independent vectors is a static method. See the examples for more context. |
| [cross()](https://p5js.org/reference/#/p5.Vector/cross)      | 外積。　Calculates and returns a vector composed of the cross product between two vectors. Both the static and non static methods return a new [p5.Vector](https://p5js.org/reference/#/p5.Vector). See the examples for more context. |
| [dist()](https://p5js.org/reference/#/p5.Vector/dist)        | 距離。 Calculates the Euclidean distance between two points (considering a point as a vector object). |
| [normalize()](https://p5js.org/reference/#/p5.Vector/normalize) | 正規化，長度變成1。Normalize the vector to length 1 (make it a unit vector). |
| [limit()](https://p5js.org/reference/#/p5.Vector/limit)      | 限制向量長。 Limit the magnitude of this vector to the value used for the **max** parameter. |
| [setMag()](https://p5js.org/reference/#/p5.Vector/setMag)    | 設定向量長度。Set the magnitude of this vector to the value used for the **len** parameter. |
| [heading()](https://p5js.org/reference/#/p5.Vector/heading)  | 計算角度。僅限2D。Calculate the angle of rotation for this vector (only 2D vectors) |
| [rotate()](https://p5js.org/reference/#/p5.Vector/rotate)    | 計算角度旋轉後的向量。僅限2D。Rotate the vector by an angle (only 2D vectors), magnitude remains the same |
| [angleBetween()](https://p5js.org/reference/#/p5.Vector/angleBetween) | 向量間夾角。　Calculates and returns the angle (in radians) between two vectors. |
| [lerp()](https://p5js.org/reference/#/p5.Vector/lerp)        | 線性內插。 Linear interpolate the vector to another vector   |
| [array()](https://p5js.org/reference/#/p5.Vector/array)      | 向量轉陣列，方便計算。Return a representation of this vector as a float array. This is only for temporary use. If used in any other fashion, the contents should be copied by using the **p5.Vector.[copy()](https://p5js.org/reference/#/p5.Vector/copy)** method to copy into your own array. |
| [equals()](https://p5js.org/reference/#/p5.Vector/equals)    | Equality check against a [p5.Vector](https://p5js.org/reference/#/p5.Vector) |
| [fromAngle()](https://p5js.org/reference/#/p5.Vector/fromAngle) | Make a new 2D vector from an angle                           |
| [fromAngles()](https://p5js.org/reference/#/p5.Vector/fromAngles) | Make a new 3D vector from a pair of ISO spherical angles     |
| [random2D()](https://p5js.org/reference/#/p5.Vector/random2D) | Make a new 2D unit vector from a random angle                |
| [random3D()](https://p5js.org/reference/#/p5.Vector/random3D) | Make a new random 3D unit vector.                            |

## Motions of a ball

在以向量為基礎的運動，我們用一個 ball 開始。

線性運動，碰撞反彈，有角度的運動，沿著圓運動，沿著曲線運動。

## Motions of an arrow (directional)

先繪出一個箭頭，進行運動。箭頭要朝向運動方向。角度需計算。

## Motions with image

沿著曲線運動的自行車。

## Motions in cycle ( circle, sin, 鐘擺)

好玩的 sin 。

## 陣列與物件導向



```

```



## frameCount & frameSave

利用 frameCount 或是 millis() 。

將動畫轉成影片。